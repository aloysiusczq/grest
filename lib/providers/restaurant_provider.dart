import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:grest/models/menu.dart';
import 'package:grest/models/restaurants.dart';

class RestaurantProvider extends ChangeNotifier {
  FirebaseFirestore firestoreInstance = FirebaseFirestore.instance;

  FirebaseAuth auth = FirebaseAuth.instance;

  CollectionReference resCollection =
      FirebaseFirestore.instance.collection("restaurants");

  CollectionReference reserveCollection =
      FirebaseFirestore.instance.collection("reservations");

  // get restaurant details from firestore
  Stream<List<Restaurant>> get listofRestauants {
    return resCollection.snapshots().map(resListFromSnapshot);
  }

  // get list of restaurants
  List<Restaurant> resListFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.docs.map((e) {
      return Restaurant(
        restaurantId: e.id,
        restaurantName: e.data()["restaurantName"] ?? "",
        restaurantAddress: e.data()["restaurantAddress"] ?? "",
        restaurantLocation: e.data()["restaurantLocation"] ?? "",
        restaurantOperateHrs: e.data()["restaurantOperateHrs"] ?? "",
        restaurantContactNumber: e.data()["restaurantContactNumber"] ?? "",
        restaurantCuisineTypes: e.data()["restaurantCuisineTypes"] ?? "",
        restaurantPriceRange: e.data()["restaurantPriceRange"] ?? "",
        restaurantLat: e.data()["restaurantLat"] ?? "",
        restaurantLong: e.data()["restaurantLong"] ?? "",
        restaurantPicUrl: e.data()["restaurantPicUrl"] ?? "",
      );
    }).toList();
  }

  Stream<List<Menu>> get theMenu {
    return resCollection
        .doc("4xPGcF1qzvB2uYnUev21")
        .collection("menus")
        .snapshots()
        .map(menuListFromSnapshot);
  }

  List<Menu> menuListFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.docs.map((e) {
      List value = e.data()["food"];
      print(value);
      return Menu(
        menuId: e.id,
        foodName: e.data()["food"]["foodName"] ?? "",
        foodPicUrl: e.data()["food"]["foodPicUrl"] ?? "",
        foodPrice: e.data()["food"]["foodPrice"] ?? "",
      );
    }).toList();
  }

  // get restaurant details from firestore
  Stream<Restaurant> get restaurantDetails {
    return resCollection.doc().snapshots().map(_resDataFromSnapShot);
  }

  //restaurant data from snapshot
  Restaurant _resDataFromSnapShot(DocumentSnapshot snapshot) {
    return Restaurant(
      restaurantId: snapshot.reference.id,
      restaurantName: snapshot.data()["restaurantName"],
      restaurantAddress: snapshot.data()["restaurantAddress"],
      restaurantLocation: snapshot.data()["restaurantLocation"],
      restaurantOperateHrs: snapshot.data()["restaurantOperateHrs"],
      restaurantContactNumber: snapshot.data()["restaurantPriceRange"],
      restaurantCuisineTypes: snapshot.data()["restaurantCuisineTypes"],
      restaurantPriceRange: snapshot.data()["restaurantPriceRange"],
      restaurantLat: snapshot.data()["restaurantLat"] ?? "",
      restaurantLong: snapshot.data()["restaurantLong"] ?? "",
      restaurantPicUrl: snapshot.data()["restaurantPicUrl"] ?? "",
    );
  }

  // create user review
  Future createUserReview(
      {BuildContext context,
      String currentId,
      String userId,
      String userName,
      String submissiondt,
      String review}) {
    try {
      return resCollection.doc(currentId).collection("reviews").add({
        "userId": userId,
        "review": review,
        "submitTime": submissiondt,
      });
    } catch (e) {
      return showDialog(
          context: context,
          child: AlertDialog(
            title: Text("Dear $userName",
                style: GoogleFonts.montserrat(
                    fontSize: 22, fontWeight: FontWeight.w500)),
            content: Text("The review has not submitted successfully.",
                style: GoogleFonts.montserrat(
                    fontSize: 18, fontWeight: FontWeight.w300)),
            actions: [
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text("Try again"))
            ],
          ));
    }
  }

  //update table status
  Future updateTableStatus(
      {BuildContext context, String currentId, String tableId}) {
    try {
      return resCollection
          .doc(currentId)
          .collection("layouts")
          .doc(tableId)
          .update({"status": "unavailable"}).then((_) async {
        return Future.value(true);
      });
    } catch (e) {
      print("e :$e");
      return e;
    }
  }

// create new reservation
  Future createReservation({
    BuildContext context,
    String currentUserId,
    String currentUserName,
    String currentResId,
    String tableNumber,
    String specialRequest,
    String adultNumber,
    String childrenNumber,
    String submissiondt,
  }) {
    try {
      return reserveCollection.doc().set({
        "userId": currentUserId,
        "restaurantId": currentResId,
        "tableNumber": tableNumber,
        "specialRequest": specialRequest,
        "adultNumber": adultNumber,
        "childrenNumber": childrenNumber,
        "submitTime": submissiondt,
      }).then((value) => showDialog(
          context: context,
          child: AlertDialog(
            title: Text("Dear $currentUserName",
                style: GoogleFonts.montserrat(
                    fontSize: 22, fontWeight: FontWeight.w500)),
            content: Text("You have successfully reserved Table $tableNumber",
                style: GoogleFonts.montserrat(
                    fontSize: 18, fontWeight: FontWeight.w300)),
            actions: [
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text("Okay"))
            ],
          )));
    } catch (e) {
      print("eR :$e");
      return e;
    }
  }
}
