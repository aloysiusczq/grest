import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:grest/models/reservation.dart';

class ReservationProvider extends ChangeNotifier {
  FirebaseFirestore firestoreInstance = FirebaseFirestore.instance;

  CollectionReference reserveCollection =
      FirebaseFirestore.instance.collection("reservations");

  // get restaurant details from firestore
  Stream<List<Reservation>> get listofReservations {
    return reserveCollection.snapshots().map(reserveListFromSnapshot);
  }

  // get list of restaurants
  List<Reservation> reserveListFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.docs.map((e) {
      return Reservation(
        reserveId: e.id,
        restaurantId: e.data()["restaurantId"],
        specialRequest: e.data()["specialRequest"] ?? "",
        submitTime: e.data()["submitTime"] ?? "",
        tableNumber: e.data()["tableNumber"] ?? "",
        userId: e.data()["userId"] ?? "",
      );
    }).toList();
  }
}
