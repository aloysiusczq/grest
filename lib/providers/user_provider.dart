import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:grest/models/user.dart';
import 'package:grest/screens/login_screen.dart';
import 'package:grest/utils/globals.dart';

class UserProvider extends ChangeNotifier {
  Timer _timer;
  FirebaseAuth auth = FirebaseAuth.instance;
  User user;

  final CollectionReference userCollection =
      FirebaseFirestore.instance.collection("users");

  // create user obj based on firebase user
  UserBody _userFromFirebaseUser(user) {
    return user != null
        ? UserBody(
            userId: user.uid,
            email: user.email,
          )
        : null;
  }

  // sign in
  Future loginUser(
      {BuildContext context, String email, String password}) async {
    try {
      UserCredential userCredential = await auth.signInWithEmailAndPassword(
          email: email, password: password);
      user = userCredential.user;
      print(
          "user : ${user.uid} + ${user.displayName}"); // authentication details
      currentUserId = user.uid;
      return _userFromFirebaseUser(user);
    } catch (error) {
      showDialog(
          context: context,
          child: AlertDialog(
            title: Text("Dear User,",
                style: GoogleFonts.montserrat(
                    fontSize: 22, fontWeight: FontWeight.w500)),
            content: Text(
                "There is no user with such entries. Please try again.",
                style: GoogleFonts.montserrat(
                    fontSize: 18, fontWeight: FontWeight.w300)),
            actions: [
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text("Okay"))
            ],
          ));
      print(error.toString());
      return null;
    }
  }

  // register
  Future registerUser(
      {BuildContext context,
      String userName,
      String email,
      String password}) async {
    try {
      UserCredential userCredential = await auth.createUserWithEmailAndPassword(
          email: email, password: password);
      user = userCredential.user;
      await updateUserProfile(userName: userName, contactNumber: "");
      return _userFromFirebaseUser(user);
    } catch (error) {
      showDialog(
          context: context,
          child: AlertDialog(
            title: Text("Dear User,",
                style: GoogleFonts.montserrat(
                    fontSize: 22, fontWeight: FontWeight.w500)),
            // content: Text(error.toString()),
            content: Text(
                "The email address is already in use by another account.",
                style: GoogleFonts.montserrat(
                    fontSize: 18, fontWeight: FontWeight.w300)),
            actions: [
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text("Okay"))
            ],
          ));
      print("e ${error.toString()}");
      return null;
    }
  }

  // sign out
  Future signOut(context, String username) async {
    try {
      return await auth
          .signOut()
          .then((value) => auth.authStateChanges().listen((user) {
                if (user == null) {
                  print('User is signed out!');
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        _timer = Timer(Duration(seconds: 3), () {
                          Navigator.of(context).pushReplacement(
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      LoginScreen(exiting: true)));
                        });
                        return AlertDialog(
                          title: Text("Dear $username,",
                              style: GoogleFonts.montserrat(
                                  fontSize: 22, fontWeight: FontWeight.w500)),
                          content: Text("You have successfully logged out.",
                              style: GoogleFonts.montserrat(
                                  fontSize: 18, fontWeight: FontWeight.w300)),
                        );
                      });
                } else {
                  print('User not yet signed out!');
                }
              }));
    } catch (error) {
      print(error.toString());
      return null;
    }
  }

  //create collection
  Future updateUserProfile({String userName, String contactNumber}) async {
    return await userCollection.doc(user.uid).set({
      'username': userName,
      "contactNumber": contactNumber,
      "favourites": [],
      "profilePicUrl": ""
    });
  }

  //get user data from fireshore
  Stream<UserBody> get userData {
    return userCollection.doc(user.uid).snapshots().map(_userDataFromSnapShot);
  }

  //user data from snapshot
  UserBody _userDataFromSnapShot(DocumentSnapshot snapshot) {
    return UserBody(
      userId: user.uid,
      userName: snapshot.data()["username"],
      email: user.email,
      contactNumber: snapshot.data()["contactNumber"],
      userProfilePic: snapshot.data()["profilePicUrl"],
    );
  }

  //create user favourite list
  Future createFavouriteList({String userId, String restaurantId}) {
    try {
      List<String> restaurantIdList = [restaurantId];
      return userCollection
          .doc(userId)
          .update({"favourites": FieldValue.arrayUnion(restaurantIdList)}).then(
              (value) {
        return Future.value(true);
      });
    } catch (e) {
      return e;
    }
  }

  //remove favourite
  Future removeFavourite(
      {BuildContext context, String userId, String restaurantId}) {
    List<String> restaurantIdList = [restaurantId];
    try {
      return userCollection.doc(userId).update({
        "favourites": FieldValue.arrayRemove(restaurantIdList)
      }).then((value) {
        return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              content: Text("The restaurant is deleted successfully",
                  style: GoogleFonts.montserrat(
                      fontSize: 18, fontWeight: FontWeight.w300)),
              actions: [
                FlatButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text("Okay"))
              ],
            );
          },
        );
      });
    } catch (e) {
      return e;
    }
  }

  // upload profile picture
  Future uploadProfilePicture({String imageUrl, String userId}) {
    try {
      return userCollection
          .doc(userId)
          .update({"profilePicUrl": imageUrl}).then((value) {
        return Future.value(true);
      });
    } catch (e) {
      return e;
    }
  }
}
