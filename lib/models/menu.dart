class Menu {
  final String menuId;
  final String foodName;
  final String foodPicUrl;
  final String foodPrice;

  Menu({
    this.menuId,
    this.foodName,
    this.foodPicUrl,
    this.foodPrice,
  });

  Map<String, dynamic> toJson() {
    return {
      "id": menuId,
      "foodName": foodName,
      "foodPicUrl": foodPicUrl,
      "foodPrice": foodPrice,
    };
  }
}
