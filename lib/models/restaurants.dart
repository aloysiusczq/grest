import 'package:grest/models/menu.dart';

class Restaurant {
  final String restaurantId;
  final String restaurantName;
  final String restaurantAddress;
  final String restaurantLocation;
  final String restaurantOperateHrs;
  final String restaurantContactNumber;
  final List restaurantCuisineTypes;
  final String restaurantPriceRange;
  final String restaurantLat;
  final String restaurantLong;
  final String restaurantPicUrl;
  final List<Menu> menu;

  Restaurant({
    this.restaurantId,
    this.restaurantName,
    this.restaurantAddress,
    this.restaurantLocation,
    this.restaurantOperateHrs,
    this.restaurantContactNumber,
    this.restaurantCuisineTypes,
    this.restaurantPriceRange,
    this.menu,
    this.restaurantLat,
    this.restaurantLong,
    this.restaurantPicUrl,
  });

  Map<String, dynamic> toJson() {
    return {
      "id": restaurantId,
      "restaurantName": restaurantName,
      "restaurantAddress": restaurantAddress,
      "restaurantLocation": restaurantLocation,
      "operatingHours": restaurantOperateHrs,
      "restaurantContactNumber": restaurantContactNumber,
      "restaurantCuisineTypes": restaurantCuisineTypes != null
          ? List.from(restaurantCuisineTypes.map((e) => e))
          : [],
      "restaurantPriceRange": restaurantPriceRange,
      "restaurantLat": restaurantLat,
      "restaurantLong": restaurantLong,
      "restaurantPicUrl": restaurantPicUrl,
      'menu': menu != null ? List.from(menu.map((b) => b)) : [],
    };
  }
}
