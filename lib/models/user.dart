class UserBody {
  final String userId;
  final String userName;
  final String email;
  final String password;
  final String contactNumber;
  final String userProfilePic;

  UserBody(
      {this.userId,
      this.userName,
      this.email,
      this.password,
      this.contactNumber,
      this.userProfilePic});

  Map<String, dynamic> toJson() {
    return {
      "id": userId,
      "username": userName,
      "email": email,
      "password": password,
      "contactNumber": contactNumber,
      "userProfilePic": userProfilePic,
    };
  }
}
