class Reservation {
  final String reserveId;
  final String restaurantId;
  final String specialRequest;
  final String submitTime;
  final String tableNumber;
  final String userId;

  Reservation({
    this.reserveId,
    this.restaurantId,
    this.specialRequest,
    this.submitTime,
    this.tableNumber,
    this.userId,
  });

  Map<String, dynamic> toJson() {
    return {
      "reserveId": reserveId,
      "restaurantId": restaurantId,
      "specialRequest": specialRequest,
      "submitTime": submitTime,
      "tableNumber": tableNumber,
      "userId": userId,
    };
  }
}
