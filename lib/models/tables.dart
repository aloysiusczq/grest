class Tables {
  final String tableNumber;
  final String xPosition;
  final String yPosition;

  Tables(this.tableNumber, this.xPosition, this.yPosition);
}
