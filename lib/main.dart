import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:grest/providers/reservation_provider.dart';
import 'package:grest/providers/restaurant_provider.dart';
import 'package:grest/screens/all_categories_screen.dart';
import 'package:grest/screens/cat_res_screen.dart';
import 'package:grest/screens/home_screen.dart';
import 'package:grest/screens/new_reservation_screen.dart';
import 'package:grest/screens/new_reservation_table_screen.dart';
import 'package:grest/screens/reservation_history_screen.dart';
import 'package:grest/screens/reservation_summary.dart';
import 'package:grest/screens/restaurant_details_screen.dart';
import 'package:grest/screens/search_screen.dart';
import 'package:grest/screens/user_favourites.dart';
import 'package:grest/screens/user_profile.dart';
import 'package:grest/widgets/bottom_nav_bar.dart';
import 'package:provider/provider.dart';
import 'package:grest/providers/user_provider.dart';
import 'package:grest/screens/landing_screen.dart';
import 'package:grest/screens/login_screen.dart';
import 'package:grest/screens/register_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

// This widget is the root of your application.
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<UserProvider>(create: (_) => UserProvider()),
        ChangeNotifierProvider<RestaurantProvider>(
            create: (_) => RestaurantProvider()),
        ChangeNotifierProvider<ReservationProvider>(
            create: (_) => ReservationProvider()),
      ],
      child: MaterialApp(
        title: 'tryst',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: LandingScreen(),
        routes: {
          LandingScreen.routeName: (_) => LandingScreen(),
          RegisterScreen.routeName: (_) => RegisterScreen(),
          LoginScreen.routeName: (_) => LoginScreen(),
          "/login_screen": (_) => LoginScreen(),
          HomeScreen.routeName: (_) => HomeScreen(),
          AllCatsScreen.routeName: (_) => AllCatsScreen(),
          SearchScreen.routeName: (_) => SearchScreen(),
          ResDetailScreen.routeName: (_) => ResDetailScreen(),
          NewResScreen.routeName: (_) => NewResScreen(),
          NewResTableScreen.routeName: (_) => NewResTableScreen(),
          ResHisScreen.routeName: (_) => RegisterScreen(),
          UserProfileScreen.routeName: (_) => UserProfileScreen(),
          NavTab.routeName: (_) => NavTab(),
          ResSumScreen.routeName: (_) => ResSumScreen(),
          UserFavScreen.routeName: (_) => UserFavScreen(),
          CatsResScreen.routeName: (_) => CatsResScreen(),
        },
        // onGenerateRoute: (settings) {
        //   return null;
        // },
      ),
    );
  }
}
