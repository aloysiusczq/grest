import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:grest/utils/globals.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:grest/models/reservation.dart';
import 'package:grest/models/restaurants.dart';
import 'package:grest/models/user.dart';
import 'package:grest/providers/reservation_provider.dart';
import 'package:grest/providers/restaurant_provider.dart';
import 'package:grest/providers/user_provider.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

class ResHisScreen extends StatefulWidget {
  static String routeName = "res_his_screen";
  final String userId;

  const ResHisScreen({Key key, this.userId}) : super(key: key);
  @override
  State<StatefulWidget> createState() => _ResHisScreen();
}

class _ResHisScreen extends State<ResHisScreen>
    with SingleTickerProviderStateMixin {
  TabController mainTabController;
  // List<String> mainTabTitle = ["Current",  "Past"];
  List<Tab> mainTabTitle = [Tab(text: "Current"), Tab(text: "Past")];

  ReservationProvider reserveProvider;
  CollectionReference userCollection =
      FirebaseFirestore.instance.collection("users");
  List<Reservation> reservations;

  RestaurantProvider restaurantProvider;
  Restaurant restaurant;
  CollectionReference resCollection =
      FirebaseFirestore.instance.collection("restaurants");

  UserProvider userProvider;
  UserBody user;

  //get today's date
  String todayDate =
      DateFormat("yyyy-MM-dd 00:00:00").format(DateTime.now()).toString();

  @override
  void initState() {
    mainTabController =
        new TabController(length: mainTabTitle.length, vsync: this);
    mainTabController
      ..addListener(() {
        if (mainTabController.indexIsChanging) {
          setState(() {});
        }
      });
    super.initState();
    print("currentUserId $currentUserId");
  }

  @override
  dispose() {
    mainTabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    reserveProvider = Provider.of(context);
    restaurantProvider = Provider.of(context);
    userProvider = Provider.of(context);
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: Color(0xffF5F5DC),
        body:
            CustomScrollView(physics: NeverScrollableScrollPhysics(), slivers: [
          SliverPersistentHeader(
            pinned: true,
            floating: true,
            delegate: CustomSliverDelegate(
              mainTabController: mainTabController,
              mainTabTitle: mainTabTitle,
              expandedHeight: screenSize.height * 0.3,
              screenSize: screenSize,
            ),
          ),
          SliverFillRemaining(
            child: Padding(
              padding: EdgeInsets.all(15.0),
              child: TabBarView(
                physics: NeverScrollableScrollPhysics(),
                controller: mainTabController,
                children: [
                  //current user reservations
                  Center(
                    child: StreamBuilder(
                        stream: reserveProvider.reserveCollection
                            .where("userId", isEqualTo: currentUserId)
                            .where("submitTime",
                                isGreaterThanOrEqualTo: todayDate)
                            .snapshots(),
                        builder: (context, snapshot) {
                          if (!snapshot.hasData) {
                            return Center(
                                child: Text("No current reservation"));
                          } else {
                            if (snapshot.data.docs.isEmpty) {
                              return Center(
                                  child: Text("No current reservation"));
                            } else {
                              return ListView.builder(
                                itemCount: snapshot.data.docs.length,
                                itemBuilder: (BuildContext context, int index) {
                                  DocumentSnapshot reservations =
                                      snapshot.data.docs[index];
                                  return currentReservation(
                                      screenSize, reservations);
                                },
                              );
                            }
                          }
                        }),
                  ),
                  //past user reservations
                  Center(
                    child: StreamBuilder(
                        stream: reserveProvider.reserveCollection
                            .where("userId", isEqualTo: currentUserId)
                            .where("submitTime", isLessThan: todayDate)
                            .snapshots(),
                        builder: (context, snapshot) {
                          if (!snapshot.hasData) {
                            return Center(child: CircularProgressIndicator());
                          } else {
                            if (snapshot.data.docs.isEmpty) {
                              return Center(child: Text("No past reservation"));
                            } else {
                              return ListView.builder(
                                itemCount: snapshot.data.docs.length,
                                itemBuilder: (BuildContext context, int index) {
                                  DocumentSnapshot reservations =
                                      snapshot.data.docs[index];
                                  return currentReservation(
                                      screenSize, reservations);
                                },
                              );
                            }
                          }
                        }),
                  ),
                ],
              ),
            ),
          )
        ]));
  }

  Widget currentReservation(Size screenSize, DocumentSnapshot reservation) {
    return Padding(
      padding: EdgeInsets.all(8.0),
      child: Container(
        height: screenSize.height * 0.18,
        width: screenSize.width * 0.8,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30), color: Colors.white),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 8.0),
          child: Center(
            child: StreamBuilder(
                stream:
                    resCollection.doc(reservation["restaurantId"]).snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return Center(child: CircularProgressIndicator());
                  } else {
                    DocumentSnapshot documentSnapshot = snapshot.data;
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          height: screenSize.height * 0.1,
                          width: screenSize.width * 0.2,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                image: NetworkImage(documentSnapshot
                                    .data()["restaurantPicUrl"]),
                                fit: BoxFit.cover,
                              ),
                              color: Colors.blue,
                              borderRadius: BorderRadius.circular(15)),
                        ),
                        SingleChildScrollView(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                width: screenSize.width * 0.55,
                                child: Text(
                                    documentSnapshot.data()["restaurantName"],
                                    softWrap: true,
                                    maxLines: 2,
                                    style: GoogleFonts.montserrat(
                                        fontSize: 20,
                                        fontWeight: FontWeight.w500,
                                        color: Color(0xff585847))),
                              ),
                              Text(reservation["submitTime"],
                                  style: GoogleFonts.raleway(
                                      fontSize: 17,
                                      fontWeight: FontWeight.w300,
                                      color: Color(0xff707070))),
                              Text(
                                  "${reservation["adultNumber"]} Adults, ${reservation["childrenNumber"]} Children",
                                  style: GoogleFonts.raleway(
                                      fontSize: 17,
                                      fontWeight: FontWeight.w300,
                                      color: Color(0xff707070))),
                              Text("Table ${reservation["tableNumber"]}",
                                  style: GoogleFonts.raleway(
                                      fontSize: 17,
                                      fontWeight: FontWeight.w300,
                                      color: Color(0xff707070))),
                              StreamBuilder(
                                  stream: userProvider.userCollection
                                      .doc(reservation["userId"])
                                      .snapshots(),
                                  builder: (context, snapshot) {
                                    if (!snapshot.hasData) {
                                      return Center(
                                          child: CircularProgressIndicator());
                                    } else {
                                      DocumentSnapshot documentSnapshot =
                                          snapshot.data;
                                      String name =
                                          documentSnapshot.data()["username"];
                                      return Container(
                                        width: screenSize.width * 0.55,
                                        child: Text(name,
                                            style: GoogleFonts.raleway(
                                                fontSize: 17,
                                                fontWeight: FontWeight.w300,
                                                color: Color(0xff707070))),
                                      );
                                    }
                                  }),
                            ],
                          ),
                        )
                      ],
                    );
                  }
                }),
          ),
        ),
      ),
    );
  }
}

class CustomSliverDelegate extends SliverPersistentHeaderDelegate {
  final double expandedHeight;
  final Size screenSize;
  final TabController mainTabController;
  final List<Tab> mainTabTitle;
  CustomSliverDelegate({
    @required this.expandedHeight,
    this.screenSize,
    this.mainTabController,
    this.mainTabTitle,
  });

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Stack(
      fit: StackFit.expand,
      overflow: Overflow.visible,
      children: [
        Container(
          padding: EdgeInsets.symmetric(vertical: screenSize.height * 0.1),
          width: screenSize.width,
          decoration: BoxDecoration(
              color: Color(0xffA8A887),
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(50),
                  bottomRight: Radius.circular(50))),
          child: Center(
            child: Text("Recent Activities",
                style: GoogleFonts.montserrat(
                    fontWeight: FontWeight.w600,
                    fontSize: 30,
                    color: Colors.white)),
          ),
        ),
        Positioned(
          top: expandedHeight / 1.18 - shrinkOffset,
          left: screenSize.width / 9,
          child: Opacity(
              opacity: (1 - shrinkOffset / expandedHeight),
              child: Container(
                height: screenSize.height * 0.08,
                width: screenSize.width / 1.3,
                decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Color(0xff707070),
                        blurRadius: 8,
                      ),
                    ],
                    borderRadius: BorderRadius.circular(30)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    TabBar(
                        controller: mainTabController,
                        isScrollable: true,
                        indicatorWeight: 3.0,
                        // indicatorColor: Color(0xffA887A6),
                        indicatorColor: Colors.transparent,
                        unselectedLabelColor: Colors.grey,
                        tabs: [
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 25.0),
                            child: Tab(
                              child: Text(mainTabTitle[0].text,
                                  style: GoogleFonts.montserrat(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w700,
                                      color: mainTabController.index == 0
                                          ? Color(0xffA887A6)
                                          : Color(0xffD3D3D3))),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 25.0),
                            child: Tab(
                              child: Text(mainTabTitle[1].text,
                                  style: GoogleFonts.montserrat(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w700,
                                      color: mainTabController.index == 1
                                          ? Color(0xffA887A6)
                                          : Color(0xffD3D3D3))),
                            ),
                          ),
                        ]),
                  ],
                ),
              )),
        ),
      ],
    );
  }

  @override
  double get maxExtent => expandedHeight;

  @override
  double get minExtent => kToolbarHeight;

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }
}
