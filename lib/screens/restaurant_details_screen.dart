import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:grest/models/menu.dart';
import 'package:grest/models/restaurants.dart';
import 'package:grest/models/user.dart';
import 'package:grest/providers/restaurant_provider.dart';
import 'package:grest/providers/user_provider.dart';
import 'package:grest/screens/new_reservation_screen.dart';
import 'package:grest/widgets/map_sheet.dart';
import 'package:image_picker/image_picker.dart';
import 'package:map_launcher/map_launcher.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

class ResDetailScreen extends StatefulWidget {
  static String routeName = "res_datail_screen";

  final Restaurant currentRestaurant;
  final UserBody currentUser;

  const ResDetailScreen({Key key, this.currentRestaurant, this.currentUser})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _ResDetailScreen();
}

class _ResDetailScreen extends State<ResDetailScreen>
    with TickerProviderStateMixin {
  TabController _mainTabController;
  List<String> mainTabTitle = ["Details", "Our Menu", "Reviews"];

  final _userReviewKey = new GlobalKey<FormState>();
  TextEditingController userReviewTEC = TextEditingController();
  Color borderSideColor = Color(0xffA8A887);

  RestaurantProvider resProvider;
  List<Menu> ymenu = [];

  List<bool> isClick = [];

  UserProvider userProvider;

  final _picker = ImagePicker();
  String menuId;

  @override
  void initState() {
    _mainTabController =
        new TabController(length: mainTabTitle.length, vsync: this);
    _mainTabController
      ..addListener(() {
        if (_mainTabController.indexIsChanging) {
          setState(() {});
        }
      });
    print(widget.currentRestaurant.restaurantId);
    super.initState();
  }

  @override
  dispose() {
    _mainTabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    resProvider = Provider.of(context);
    userProvider = Provider.of(context);
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Color(0xffFFFFF0),
      body: Column(
        children: [
          StreamBuilder(
              stream: resProvider.resCollection
                  .doc(widget.currentRestaurant.restaurantId)
                  .snapshots(),
              builder: (context, snapshot) {
                DocumentSnapshot documentSnapshot = snapshot.data;
                return Container(
                  height: screenSize.height * 0.27,
                  width: screenSize.width,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(25.0),
                      bottomLeft: Radius.circular(25.0),
                    ),
                    image: snapshot.hasData
                        ? DecorationImage(
                            image: NetworkImage(
                                documentSnapshot.data()["restaurantPicUrl"]),
                            fit: BoxFit.cover,
                            colorFilter: ColorFilter.mode(
                                Color(0x33000000), BlendMode.srcOver))
                        : null,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 30.0, horizontal: 18.0),
                          child: CircleAvatar(
                            backgroundColor: Colors.white,
                            radius: 22.0,
                            child:
                                Icon(Icons.arrow_back_ios_outlined, size: 20.0),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 8.0),
                        child: ListTile(
                          title: Text(
                            widget.currentRestaurant.restaurantName,
                            style: GoogleFonts.montserrat(
                                fontSize: 30,
                                fontWeight: FontWeight.w500,
                                color: Colors.white),
                          ),
                          subtitle: Text(
                            widget.currentRestaurant.restaurantLocation,
                            style: GoogleFonts.montserrat(
                                fontSize: 18,
                                fontWeight: FontWeight.w400,
                                color: Colors.white),
                          ),
                          trailing: InkWell(
                            onTap: () async {
                              dynamic result =
                                  await userProvider.createFavouriteList(
                                userId: widget.currentUser.userId,
                                restaurantId:
                                    widget.currentRestaurant.restaurantId,
                              );
                              if (result == true) {
                                print(result);
                              } else {
                                print("failed");
                              }
                            },
                            child: StreamBuilder(
                                stream: userProvider.userCollection
                                    .doc(widget.currentUser.userId)
                                    .snapshots(),
                                builder: (context, snapshot) {
                                  if (!snapshot.hasData) {
                                    return CircleAvatar(
                                      backgroundColor: Colors.white,
                                      radius: 22.0,
                                      child: Icon(Icons.bookmark_border,
                                          size: 25.0, color: Colors.grey[600]),
                                    );
                                  } else {
                                    if (snapshot.data
                                        .data()['favourites']
                                        .contains(widget
                                            .currentRestaurant.restaurantId)) {
                                      print("got");
                                      return CircleAvatar(
                                        backgroundColor: Colors.white,
                                        radius: 22.0,
                                        child: Icon(Icons.bookmark,
                                            size: 25.0,
                                            color: Color(0xffA887A6)),
                                      );
                                    } else {
                                      print("dont have");
                                      return CircleAvatar(
                                        backgroundColor: Colors.white,
                                        radius: 22.0,
                                        child: Icon(Icons.bookmark_border,
                                            size: 25.0,
                                            color: Colors.grey[600]),
                                      );
                                    }
                                  }
                                }),
                          ),
                        ),
                      )
                    ],
                  ),
                );
              }),
          Expanded(
            child: Container(
              margin: EdgeInsets.symmetric(vertical: screenSize.height * 0.02),
              width: screenSize.width,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(30.0),
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xAC000000),
                      blurRadius: 8,
                    ),
                  ]),
              child: Column(
                children: [
                  Expanded(
                    child: TabBarView(
                        physics: NeverScrollableScrollPhysics(),
                        controller: _mainTabController,
                        children: [
                          details(screenSize),
                          menu(screenSize),
                          reviews(screenSize),
                        ]),
                  ),
                  TabBar(
                    controller: _mainTabController,
                    isScrollable: true,
                    indicator: UnderlineTabIndicator(
                      borderSide:
                          BorderSide(color: Color(0xffA887A6), width: 3.0),
                      insets: EdgeInsets.fromLTRB(
                          0.0, 0.0, 0.0, screenSize.height * 0.057),
                    ),
                    tabs: <Widget>[
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 25.0),
                        child: Tab(
                          child: Text(mainTabTitle[0],
                              style: GoogleFonts.montserrat(
                                  fontWeight: _mainTabController.index == 0
                                      ? FontWeight.w700
                                      : FontWeight.w400,
                                  color: _mainTabController.index == 0
                                      ? Color(0xffA887A6)
                                      : Color(0xffD3D3D3))),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 25.0),
                        child: Tab(
                          child: Text(mainTabTitle[1],
                              style: GoogleFonts.montserrat(
                                  fontWeight: _mainTabController.index == 1
                                      ? FontWeight.w700
                                      : FontWeight.w400,
                                  color: _mainTabController.index == 1
                                      ? Color(0xffA887A6)
                                      : Color(0xffD3D3D3))),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 25.0),
                        child: Tab(
                          child: Text(mainTabTitle[2],
                              style: GoogleFonts.montserrat(
                                  fontWeight: _mainTabController.index == 2
                                      ? FontWeight.w700
                                      : FontWeight.w400,
                                  color: _mainTabController.index == 2
                                      ? Color(0xffA887A6)
                                      : Color(0xffD3D3D3))),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: screenSize.height * 0.12,
            width: screenSize.width,
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: Color(0xff707070)),
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20.0),
                topRight: Radius.circular(20.0),
              ),
            ),
            child: Center(
                child: InkWell(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return NewResScreen(
                            currentUser: widget.currentUser,
                            selectedRes: widget.currentRestaurant);
                      }));
                    },
                    child: reserveButton(screenSize))),
          )
        ],
      ),
    );
  }

  Future chooseProfilePic() async {
    final pickedFile = await _picker.getImage(source: ImageSource.gallery);
    return pickedFile;
  }

  Future<String> uploadFile(File _image) async {
    FirebaseStorage storageReference = FirebaseStorage.instance;
    Reference ref =
        storageReference.ref().child("menuimage" + DateTime.now().toString());
    TaskSnapshot taskSnapshot = await ref.putFile(_image);
    String returnURL;
    if (taskSnapshot.state == TaskState.success) {
      returnURL = await taskSnapshot.ref.getDownloadURL();
      print("returnURL $returnURL");
    }
    return returnURL;
  }

  Widget reserveButton(Size screenSize) {
    return Container(
        height: screenSize.height * 0.07,
        width: screenSize.width * 0.8,
        decoration: BoxDecoration(
            border: Border.all(color: Colors.black),
            borderRadius: BorderRadius.circular(50.0),
            color: Color(0xff8788A8)),
        child: Center(
          child: Text("Reserve",
              style: GoogleFonts.montserrat(
                  fontSize: 22,
                  fontWeight: FontWeight.w300,
                  color: Colors.white)),
        ));
  }

  Widget details(Size screenSize) {
    return Column(
      children: [
        InkWell(
            onTap: () async {
              MapsSheet.show(
                  context: context,
                  onMapTap: (map) {
                    map.showMarker(
                      // Coords(lat,long)
                      coords: Coords(
                          double.parse(widget.currentRestaurant.restaurantLat),
                          double.parse(
                              widget.currentRestaurant.restaurantLong)),
                      title: widget.currentRestaurant.restaurantName,
                    );
                    // map.showDirections(
                    //   destinationTitle: "Sharing Planet",
                    //   destination: Coords(
                    //       double.parse(widget.currentRestaurant.restaurantLat),
                    //       double.parse(widget.currentRestaurant.restaurantLong)),
                    // );
                  });
            },
            child: Padding(
              padding: const EdgeInsets.all(18.0),
              child: Container(
                  height: screenSize.height * 0.1,
                  width: screenSize.width * 0.8,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15),
                    boxShadow: [
                      BoxShadow(blurRadius: 5, color: Color(0x3C000000))
                    ],
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          boxShadow: [
                            BoxShadow(blurRadius: 5, color: Color(0x3C000000))
                          ],
                        ),
                        child: CircleAvatar(
                            backgroundColor: Colors.white,
                            radius: 22.0,
                            child: Icon(
                              Icons.map_outlined,
                              size: 20,
                              color: Colors.black,
                            )),
                      ),
                      Text("Show me how to reach",
                          maxLines: 2,
                          style: GoogleFonts.raleway(
                              fontSize: 15,
                              fontWeight: FontWeight.w800,
                              color: Color(0xff707070)))
                    ],
                  )),
            )),
        Expanded(
          child: SingleChildScrollView(
            child: Column(
              children: [
                ListTile(
                    leading: Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        boxShadow: [
                          BoxShadow(blurRadius: 5, color: Color(0x3C000000))
                        ],
                      ),
                      child: CircleAvatar(
                          backgroundColor: Colors.white,
                          radius: 22.0,
                          child: Icon(
                            Icons.location_on_rounded,
                            size: 20,
                            color: Colors.black,
                          )),
                    ),
                    title: Text(widget.currentRestaurant.restaurantAddress,
                        maxLines: 2,
                        style: GoogleFonts.raleway(
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                            color: Color(0xff707070)))),
                ListTile(
                    leading: Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        boxShadow: [
                          BoxShadow(blurRadius: 5, color: Color(0x3C000000))
                        ],
                      ),
                      child: CircleAvatar(
                          backgroundColor: Colors.white,
                          radius: 22.0,
                          child: Icon(
                            Icons.access_time,
                            size: 20,
                            color: Colors.black,
                          )),
                    ),
                    title: Text(widget.currentRestaurant.restaurantOperateHrs,
                        maxLines: 2,
                        style: GoogleFonts.raleway(
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                            color: Color(0xff707070)))),
                ListTile(
                    leading: Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        boxShadow: [
                          BoxShadow(blurRadius: 5, color: Color(0x3C000000))
                        ],
                      ),
                      child: CircleAvatar(
                          backgroundColor: Colors.white,
                          radius: 22.0,
                          child: Icon(
                            Icons.phone,
                            size: 20,
                            color: Colors.black,
                          )),
                    ),
                    title: Text(
                        widget.currentRestaurant.restaurantContactNumber,
                        maxLines: 2,
                        style: GoogleFonts.raleway(
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                            color: Color(0xff707070)))),
                ListTile(
                    leading: Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        boxShadow: [
                          BoxShadow(blurRadius: 5, color: Color(0x3C000000))
                        ],
                      ),
                      child: CircleAvatar(
                          backgroundColor: Colors.white,
                          radius: 22.0,
                          child: Icon(
                            Icons.restaurant,
                            size: 20,
                            color: Colors.black,
                          )),
                    ),
                    title: Text(
                        textSelect(widget
                            .currentRestaurant.restaurantCuisineTypes
                            .toString()),
                        maxLines: 2,
                        style: GoogleFonts.raleway(
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                            color: Color(0xff707070)))),
                ListTile(
                    leading: Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        boxShadow: [
                          BoxShadow(blurRadius: 5, color: Color(0x3C000000))
                        ],
                      ),
                      child: CircleAvatar(
                          backgroundColor: Colors.white,
                          radius: 22.0,
                          child: Icon(
                            Icons.attach_money_outlined,
                            size: 20,
                            color: Colors.black,
                          )),
                    ),
                    title: Text(widget.currentRestaurant.restaurantPriceRange,
                        maxLines: 2,
                        style: GoogleFonts.raleway(
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                            color: Color(0xff707070)))),
              ],
            ),
          ),
        ),
      ],
    );
  }

  String textSelect(String str) {
    str = str.replaceAll('[', '');
    str = str.replaceAll(']', '');
    return str;
  }

  Widget menu(Size screenSize) {
    return StreamBuilder(
        stream: resProvider.resCollection
            .doc(widget.currentRestaurant.restaurantId)
            .collection("menus")
            .snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(child: CircularProgressIndicator());
          } else {
            if (snapshot.data.docs.isEmpty) {
              return Center(child: Text("No menu"));
            } else {
              return Padding(
                padding: EdgeInsets.only(right: 10.0, left: 10.0),
                child: ListView.builder(
                    itemCount: snapshot.data.docs.length,
                    itemBuilder: (context, index) {
                      DocumentSnapshot menuData = snapshot.data.docs[index];
                      List value = menuData.data()["food"];
                      ymenu = value.map((e) {
                        return Menu(
                          menuId: menuData.id,
                          foodName: e["foodName"],
                          foodPicUrl: e["foodPicUrl"],
                          foodPrice: e["foodPrice"],
                        );
                      }).toList();
                      if (isClick.isEmpty) {
                        isClick = List.filled(snapshot.data.docs.length, false);
                      }
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          InkWell(
                              onTap: () {
                                setState(() {
                                  menuId = menuData.id;
                                  isClick[index] = !isClick[index];
                                });
                              },
                              child: Padding(
                                padding: EdgeInsets.symmetric(vertical: 8.0),
                                child: Container(
                                  width: screenSize.width * 0.8,
                                  decoration: BoxDecoration(
                                    border:
                                        Border.all(color: Color(0xff707070)),
                                    borderRadius: BorderRadius.circular(40.0),
                                    color: isClick[index] == true
                                        ? Color(0xffA8A887)
                                        : Colors.white,
                                  ),
                                  child: Center(
                                    child: Text(menuData.id,
                                        style: GoogleFonts.montserrat(
                                            fontSize: 20,
                                            fontWeight: FontWeight.w400,
                                            color: isClick[index] == true
                                                ? Colors.white
                                                : Colors.black)),
                                  ),
                                ),
                              )),
                          isClick[index] == true
                              ? Container(
                                  height: screenSize.height * 0.28,
                                  child: ListView.builder(
                                      scrollDirection: Axis.horizontal,
                                      shrinkWrap: true,
                                      itemCount: ymenu.length,
                                      itemBuilder: (context, index) {
                                        return Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Container(
                                            width: screenSize.width * 0.4,
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                                color: Colors.white,
                                                boxShadow: [
                                                  BoxShadow(
                                                    color: Color(0x3C000000),
                                                    blurRadius: 8,
                                                  ),
                                                ]),
                                            child: Column(
                                              children: [
                                                // value[index]["foodPicUrl"] == ""
                                                ymenu[index].foodPicUrl == ""
                                                    ? Container(
                                                        height:
                                                            screenSize.height *
                                                                0.15,
                                                        decoration: BoxDecoration(
                                                            border: Border.all(
                                                                color: Color(
                                                                    0x55A887A6)),
                                                            borderRadius: BorderRadius.only(
                                                                topLeft: Radius
                                                                    .circular(
                                                                        20.0),
                                                                topRight: Radius
                                                                    .circular(
                                                                        20.0))),
                                                        child: Center(
                                                            child: Icon(
                                                          Icons
                                                              .menu_book_rounded,
                                                          size: 40,
                                                          color:
                                                              Color(0xff8F76A8),
                                                        )))
                                                    : Container(
                                                        height:
                                                            screenSize.height *
                                                                0.15,
                                                        decoration:
                                                            BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius.only(
                                                                  topLeft: Radius
                                                                      .circular(
                                                                          20.0),
                                                                  topRight: Radius
                                                                      .circular(
                                                                          20.0)),
                                                          image: DecorationImage(
                                                              image: NetworkImage(
                                                                  value[index][
                                                                      "foodPicUrl"]),
                                                              fit:
                                                                  BoxFit.cover),
                                                        ),
                                                      ),
                                                Expanded(
                                                  child: Padding(
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                            vertical: 10.0,
                                                            horizontal: 10.0),
                                                    child:
                                                        SingleChildScrollView(
                                                      child: Column(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: <Widget>[
                                                          // Text(
                                                          //   value[index]
                                                          //       ["foodName"],
                                                          Text(
                                                            ymenu[index]
                                                                .foodName,
                                                            maxLines: 3,
                                                            style: GoogleFonts
                                                                .montserrat(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w500,
                                                                    fontSize:
                                                                        16.0,
                                                                    color: Color(
                                                                        0xff707070)),
                                                          ),
                                                          // Text(
                                                          //   value[index]
                                                          //       ["foodPrice"],
                                                          Text(
                                                            ymenu[index]
                                                                .foodPrice,
                                                            style: GoogleFonts.openSans(
                                                                fontSize: 13,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                                color: Color(
                                                                    0xff707070)),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        );
                                      }),
                                )
                              : Container()
                        ],
                      );
                    }),
              );
            }
          }
        });
  }

  Widget reviews(Size screenSize) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Text("Leave a review:",
                style: GoogleFonts.montserrat(
                  fontSize: 20,
                  fontWeight: FontWeight.w400,
                  color: Colors.black,
                )),
          ),
          Form(
            key: _userReviewKey,
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: screenSize.height * 0.01),
              child: textField(
                  hintText: "",
                  screenSize: screenSize,
                  textEditingController: userReviewTEC),
            ),
          ),
          FlatButton(
              color: Color(0xffA887A6),
              onPressed: () {
                // submit user review
                submitReview();
              },
              child: Text("Submit",
                  style: GoogleFonts.montserrat(
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                      color: Colors.white))),
          Divider(
            indent: screenSize.width * 0.02,
            endIndent: screenSize.width * 0.02,
            thickness: 1,
            color: Color(0xff8788A8),
          ),
          Expanded(
            child: StreamBuilder(
                stream: resProvider.resCollection
                    .doc(widget.currentRestaurant.restaurantId)
                    .collection("reviews")
                    .orderBy("submitTime", descending: true)
                    .snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return Center(child: CircularProgressIndicator());
                  } else {
                    if (snapshot.data.docs.isEmpty) {
                      return Center(child: Text("No reviews available"));
                    } else {
                      return ListView.builder(
                          padding: EdgeInsets.all(0),
                          physics: ScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: snapshot.data.docs.length,
                          itemBuilder: (BuildContext context, int index) {
                            DocumentSnapshot reviewData =
                                snapshot.data.docs[index];
                            return Padding(
                              padding: EdgeInsets.symmetric(vertical: 5.0),
                              child: Container(
                                height: screenSize.height * 0.17,
                                width: screenSize.width,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    color: Color(0xffF5F5DC)),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    ListTile(
                                      leading: StreamBuilder(
                                          stream: userProvider.userCollection
                                              .doc(reviewData["userId"])
                                              .snapshots(),
                                          builder: (context, snapshot) {
                                            if (!snapshot.hasData) {
                                              return CircleAvatar(
                                                  backgroundColor:
                                                      Colors.grey[200],
                                                  child: Icon(
                                                      Icons
                                                          .people_outline_outlined,
                                                      color:
                                                          Color(0xffA8A887)));
                                            } else {
                                              DocumentSnapshot doc =
                                                  snapshot.data;
                                              return Container(
                                                  width: 40.0,
                                                  height: 50,
                                                  decoration: BoxDecoration(
                                                      image: DecorationImage(
                                                          image: NetworkImage(doc
                                                                  .data()[
                                                              "profilePicUrl"]),
                                                          fit: BoxFit.cover),
                                                      shape: BoxShape.circle));
                                            }
                                          }),
                                      title: StreamBuilder(
                                          stream: userProvider.userCollection
                                              .doc(reviewData["userId"])
                                              .snapshots(),
                                          builder: (context, snapshot) {
                                            if (!snapshot.hasData) {
                                              return Center(
                                                  child:
                                                      CircularProgressIndicator());
                                            } else {
                                              DocumentSnapshot doc =
                                                  snapshot.data;
                                              return Text(
                                                  doc
                                                      .data()["username"]
                                                      .toString(),
                                                  style: GoogleFonts.montserrat(
                                                      fontSize: 20,
                                                      fontWeight:
                                                          FontWeight.w400));
                                            }
                                          }),
                                      subtitle: Text(reviewData["submitTime"],
                                          style: GoogleFonts.montserrat(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w300)),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: 70.0),
                                      child: Text(
                                        reviewData["review"],
                                        style: GoogleFonts.openSans(
                                            fontSize: 16,
                                            fontWeight: FontWeight.w300,
                                            color: Color(0xff707070)),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            );
                          });
                    }
                  }
                }),
          ),
        ],
      ),
    );
  }

  Future submitReview() async {
    if (_userReviewKey.currentState.validate()) {
      dynamic result = await resProvider.createUserReview(
          context: context,
          currentId: widget.currentRestaurant.restaurantId,
          userName: widget.currentUser.userName,
          userId: widget.currentUser.userId,
          review: userReviewTEC.text,
          submissiondt:
              DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now()));
      if (result != null) {
        userReviewTEC.clear();
        return showDialog(
            context: context,
            child: AlertDialog(
              title: Text("Dear ${widget.currentUser.userName}",
                  style: GoogleFonts.montserrat(
                      fontSize: 22, fontWeight: FontWeight.w500)),
              content: Text("The review has successfully submitted.",
                  style: GoogleFonts.montserrat(
                      fontSize: 18, fontWeight: FontWeight.w300)),
              actions: [
                FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text("Okay"))
              ],
            ));
      }
    }
  }

  Widget textField(
      {TextEditingController textEditingController,
      TextInputType textInputType,
      String hintText,
      Function onChanged,
      Function validator,
      Size screenSize}) {
    return Container(
      color: Color(0xffF8F8F8),
      width: screenSize.width,
      child: TextFormField(
        autofocus: false,
        textAlign: TextAlign.start,
        style:
            GoogleFonts.montserrat(fontSize: 15, fontWeight: FontWeight.w300),
        keyboardType: textInputType,
        controller: textEditingController,
        onChanged: onChanged,
        maxLines: 3,
        decoration: InputDecoration(
          hintText: hintText,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(15)),
            borderSide: BorderSide(color: borderSideColor),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(15)),
            borderSide: BorderSide(color: borderSideColor),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(15)),
            borderSide: BorderSide(color: borderSideColor),
          ),
        ),
        validator: (String value) {
          if (textEditingController == userReviewTEC) {
            if (value.isEmpty) {
              return "The review is still empty.";
            }
          }
          return null;
        },
      ),
    );
  }
}
