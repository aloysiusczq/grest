import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:grest/models/user.dart';
import 'package:grest/screens/cat_res_screen.dart';
import 'package:grest/utils/globals.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class AllCatsScreen extends StatefulWidget {
  static String routeName = "all_cats_screen";
  final UserBody currentUser;

  const AllCatsScreen({Key key, this.currentUser}) : super(key: key);
  @override
  State<StatefulWidget> createState() => _AllCatsScreen();
}

class _AllCatsScreen extends State<AllCatsScreen> {
  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Color(0xffFFFFF0),
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(
              vertical: screenSize.height * 0.02,
              horizontal: screenSize.width * 0.05),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("Categories",
                  style: GoogleFonts.montserrat(
                      fontSize: 30, fontWeight: FontWeight.w700)),
              Divider(
                thickness: 1,
                color: Color(0xff8788A8),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.only(top: screenSize.height * 0.02),
                  child: GridView.count(
                      physics: ScrollPhysics(),
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      childAspectRatio:
                          (screenSize.width * 2.75 / screenSize.height),
                      crossAxisSpacing: screenSize.width * 0.08,
                      mainAxisSpacing: screenSize.height * 0.05,
                      crossAxisCount: 2,
                      children: List.generate(categories.length, (int index) {
                        return InkWell(
                          onTap: () {
                            pushNewScreen(
                              context,
                              screen: CatsResScreen(
                                  currentUser: widget.currentUser,
                                  categoryName: categories[index]),
                              withNavBar: false,
                            );
                          },
                          child: conCategory(screenSize, catColor[index],
                              categories[index], catTextColor[index]),
                        );
                      })),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget conCategory(Size screenSize, Color bColor, String text, Color tColor) {
    return Container(
        height: screenSize.height * 0.12,
        width: screenSize.width * 0.4,
        color: bColor,
        child: Center(
          child: Text(text,
              textAlign: TextAlign.center,
              maxLines: 2,
              style: GoogleFonts.roboto(
                  fontSize: 22, fontWeight: FontWeight.w300, color: tColor)),
        ));
  }
}
