import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:grest/models/restaurants.dart';
import 'package:grest/models/user.dart';
import 'package:grest/providers/restaurant_provider.dart';
import 'package:grest/screens/reservation_summary.dart';
import 'package:provider/provider.dart';

class NewResTableScreen extends StatefulWidget {
  static String routeName = "new_res_table_screen";

  final String reserveDate;
  final String reserveTime;
  final String reserveAdult;
  final String reserveChild;
  final Restaurant reserveRes;
  final UserBody currentUser;

  const NewResTableScreen(
      {Key key,
      this.reserveDate,
      this.reserveTime,
      this.reserveAdult,
      this.reserveChild,
      this.reserveRes,
      this.currentUser})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _NewResTableScreen();
}

class _NewResTableScreen extends State<NewResTableScreen> {
  bool iconBetter;
  bool isSelect;

  RestaurantProvider resProvider;

  List<Widget> tables = [];
  String buttonText = "table 1";
  String hexColor = "A887A6";
  String tableNumber;
  String status;

  @override
  void initState() {
    super.initState();
    iconBetter = false;
    isSelect = false;
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    resProvider = Provider.of(context);
    return Scaffold(
      backgroundColor: Color(0xffFFFFF0),
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            ListTile(
              leading: Container(
                height: screenSize.height * 0.1,
                width: screenSize.width * 0.14,
                decoration: BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.circular(15),
                  image: DecorationImage(
                      image: NetworkImage(
                        widget.reserveRes.restaurantPicUrl,
                      ),
                      fit: BoxFit.cover),
                ),
              ),
              title: Text(widget.reserveRes.restaurantName,
                  style: GoogleFonts.montserrat(
                      fontSize: 25,
                      fontWeight: FontWeight.w500,
                      color: Color(0xff585847))),
              subtitle: Text(widget.reserveRes.restaurantLocation,
                  style: GoogleFonts.openSans(
                      fontSize: 20,
                      fontWeight: FontWeight.w300,
                      color: Color(0xff707070))),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: screenSize.height * 0.01),
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Padding(
                      padding:
                          EdgeInsets.only(bottom: screenSize.height * 0.02),
                      child: Container(
                        height: screenSize.height * 0.05,
                        width: screenSize.width * 0.3,
                        decoration: BoxDecoration(
                          border: Border.all(color: Color(0xffA8A887)),
                          borderRadius: BorderRadius.circular(50.0),
                          color: Color(0xffFFFFBF),
                        ),
                        child: Center(
                          child: Text(widget.reserveDate,
                              style: GoogleFonts.montserrat(
                                  fontSize: 20,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.black)),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.only(bottom: screenSize.height * 0.02),
                      child: Container(
                        height: screenSize.height * 0.05,
                        width: screenSize.width * 0.3,
                        decoration: BoxDecoration(
                          border: Border.all(color: Color(0xffA8A887)),
                          borderRadius: BorderRadius.circular(50.0),
                          color: Color(0xffFFFFBF),
                        ),
                        child: Center(
                          child: Text(widget.reserveTime,
                              style: GoogleFonts.montserrat(
                                  fontSize: 20,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.black)),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.only(bottom: screenSize.height * 0.02),
                      child: Container(
                        height: screenSize.height * 0.05,
                        width: screenSize.width * 0.3,
                        decoration: BoxDecoration(
                          border: Border.all(color: Color(0xffA8A887)),
                          borderRadius: BorderRadius.circular(50.0),
                          color: Color(0xffFFFFBF),
                        ),
                        child: Center(
                          child: Text(
                              "${int.parse(widget.reserveAdult) + int.parse(widget.reserveChild)} people",
                              style: GoogleFonts.montserrat(
                                  fontSize: 20,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.black)),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              child: Container(
                  width: screenSize.width,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30.0),
                        topRight: Radius.circular(30.0),
                      ),
                      border: Border.all(color: Color(0x3C707070))),
                  child: Column(
                    children: [
                      Expanded(
                          child: Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                //icon legend
                                InkWell(
                                  onTap: () {
                                    showDialog(
                                        context: context,
                                        child: AlertDialog(
                                          title: Text("Icon and its meaning",
                                              style: GoogleFonts.montserrat(
                                                  fontSize: 22,
                                                  fontWeight: FontWeight.w500)),
                                          content: Container(
                                            height: screenSize.height * 0.4,
                                            child: SingleChildScrollView(
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  legend(
                                                      screenSize,
                                                      true,
                                                      Icons.kitchen,
                                                      Colors.white,
                                                      "Kitchen"),
                                                  legend(
                                                      screenSize,
                                                      true,
                                                      Icons.family_restroom,
                                                      Colors.white,
                                                      "Toilet"),
                                                  legend(
                                                      screenSize,
                                                      true,
                                                      Icons
                                                          .sensor_door_outlined,
                                                      Colors.white,
                                                      "Entrance"),
                                                  legend(
                                                      screenSize,
                                                      true,
                                                      Icons.cancel_outlined,
                                                      Colors.white,
                                                      "Occupied"),
                                                  Padding(
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        vertical: 4.0),
                                                    child: Container(
                                                      height:
                                                          screenSize.height *
                                                              0.03,
                                                      width: screenSize.width *
                                                          0.3,
                                                      child: Row(
                                                        children: [
                                                          CircleAvatar(
                                                              backgroundColor:
                                                                  Color(
                                                                      0xffA887A6),
                                                              child: Text("1",
                                                                  style: GoogleFonts.montserrat(
                                                                      fontSize:
                                                                          15,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w300,
                                                                      color: Colors
                                                                          .white))),
                                                          Text("Empty",
                                                              style: GoogleFonts.openSans(
                                                                  fontSize: 15,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  color: Color(
                                                                      0xff707070)))
                                                        ],
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                          actions: [
                                            FlatButton(
                                                onPressed: () {
                                                  Navigator.pop(context);
                                                },
                                                child: Text("100% understand"))
                                          ],
                                        ));
                                  },
                                  child: Container(
                                    height: screenSize.height * 0.07,
                                    width: screenSize.width * 0.45,
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(15),
                                      boxShadow: [
                                        BoxShadow(
                                            blurRadius: 5,
                                            color: Color(0x3C000000))
                                      ],
                                    ),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        Icon(
                                          Icons.help,
                                          color: Color(0xffA887A6),
                                          size: 50,
                                        ),
                                        Container(
                                          width: screenSize.width * 0.3,
                                          child: Text("Click here to know icon",
                                              maxLines: 2,
                                              style: GoogleFonts.openSans(
                                                  fontSize: 15,
                                                  fontWeight: FontWeight.w400,
                                                  color: Color(0xff707070))),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                // table legends
                                InkWell(
                                  onTap: () {
                                    showDialog(
                                        context: context,
                                        child: AlertDialog(
                                          title: Text("Table and its occupancy",
                                              style: GoogleFonts.montserrat(
                                                  fontSize: 22,
                                                  fontWeight: FontWeight.w500)),
                                          content: Container(
                                            height: screenSize.height * 0.4,
                                            child: SingleChildScrollView(
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  table(
                                                      screenSize,
                                                      Color(0xff00FF00),
                                                      screenSize.height * 0.06,
                                                      screenSize.width * 0.12,
                                                      "Table for 1"),
                                                  table(
                                                      screenSize,
                                                      Color(0xffFC2E2E),
                                                      screenSize.height * 0.06,
                                                      screenSize.width * 0.14,
                                                      "Table for 2"),
                                                  table(
                                                      screenSize,
                                                      Color(0xffA8A887),
                                                      screenSize.height * 0.09,
                                                      screenSize.width * 0.18,
                                                      "Table for 3"),
                                                  table(
                                                      screenSize,
                                                      Color(0xffA887A6),
                                                      screenSize.height * 0.13,
                                                      screenSize.width * 0.18,
                                                      "Table for 4"),
                                                ],
                                              ),
                                            ),
                                          ),
                                          actions: [
                                            FlatButton(
                                                onPressed: () {
                                                  Navigator.pop(context);
                                                },
                                                child: Text("100% understand"))
                                          ],
                                        ));
                                  },
                                  child: Container(
                                    height: screenSize.height * 0.07,
                                    width: screenSize.width * 0.45,
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(15),
                                      boxShadow: [
                                        BoxShadow(
                                            blurRadius: 5,
                                            color: Color(0x3C000000))
                                      ],
                                    ),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        Icon(
                                          Icons.help,
                                          color: Color(0xffA8A887),
                                          size: 50,
                                        ),
                                        Container(
                                          width: screenSize.width * 0.3,
                                          child: Text(
                                              "Click here to know table",
                                              maxLines: 2,
                                              style: GoogleFonts.openSans(
                                                  fontSize: 15,
                                                  fontWeight: FontWeight.w400,
                                                  color: Color(0xff707070))),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Divider(
                              thickness: 1,
                              color: Color(0xff8788A8),
                            ),
                            StreamBuilder(
                                stream: resProvider.resCollection
                                    .doc(widget.reserveRes.restaurantId)
                                    .collection("layouts")
                                    .snapshots(),
                                builder: (context, snapshot) {
                                  if (!snapshot.hasData) {
                                    return Center(
                                        child: CircularProgressIndicator());
                                  } else {
                                    if (snapshot.data.docs.isEmpty) {
                                      return Center(
                                          child: Text("No layout available"));
                                    } else {
                                      return Expanded(
                                        child: Stack(
                                          children: addTables(
                                              snapshot.data.docs, screenSize),
                                        ),
                                      );
                                    }
                                  }
                                })
                          ],
                        ),
                      )),
                      Container(
                        height: screenSize.height * 0.12,
                        width: screenSize.width,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(color: Color(0x3C707070)),
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20.0),
                            topRight: Radius.circular(20.0),
                          ),
                        ),
                        child: InkWell(
                          onTap: tableNumber != null
                              ? () {
                                  Navigator.push(context,
                                      MaterialPageRoute(builder: (context) {
                                    return ResSumScreen(
                                      reserveDate: widget.reserveDate,
                                      reserveTime: widget.reserveTime,
                                      reserveAdult: widget.reserveAdult,
                                      reserveChild: widget.reserveChild,
                                      reserveRes: widget.reserveRes,
                                      tableNumber: tableNumber,
                                      currentUser: widget.currentUser,
                                    );
                                  }));
                                }
                              : null,
                          child: Center(
                            child: Container(
                                height: screenSize.height * 0.07,
                                width: screenSize.width * 0.8,
                                decoration: BoxDecoration(
                                    border: Border.all(color: Colors.black),
                                    borderRadius: BorderRadius.circular(25.0),
                                    color: Color(0xff8788A8)),
                                child: Center(
                                  child: Text(
                                      status != "unavailable"
                                          ? tableNumber != null
                                              ? "Book Table $tableNumber"
                                              : "Choose your table"
                                          // : "Choose another table",
                                          : setText(true),
                                      style: GoogleFonts.montserrat(
                                          fontSize: 22,
                                          fontWeight: FontWeight.w300,
                                          color: Colors.white)),
                                )),
                          ),
                        ),
                      )
                    ],
                  )),
            ),
          ],
        ),
      ),
    );
  }

  Color colorConvert(String color) {
    color = color.replaceAll("#", "");
    Color converted;
    if (color.length == 6) {
      converted = Color(int.parse("0xFF" + color));
    } else if (color.length == 8) {
      converted = Color(int.parse("0x" + color));
    }
    return converted;
  }

  setText(bool value) {
    return value == true ? "Choose another table" : "";
  }

  List<Widget> addTables(tablesData, Size screenSize) {
    tablesData.forEach((element) {
      Widget table = Positioned(
        top: double.parse(element.data()["yPosition"]),
        left: double.parse(element.data()["xPosition"]),
        child: FlatButton(
          onPressed: element.id != "entrance"
              ? element.data()["status"] == "unavailable"
                  ? () {
                      showDialog(
                          context: context,
                          child: AlertDialog(
                            title: Text("Sorry",
                                style: GoogleFonts.montserrat(
                                    fontSize: 22, fontWeight: FontWeight.w500)),
                            content: Text(
                                "This table is booked by other customer.",
                                style: GoogleFonts.montserrat(
                                    fontSize: 18, fontWeight: FontWeight.w300)),
                            actions: [
                              FlatButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: Text("Okay"))
                            ],
                          ));
                      setState(() {
                        status = element.data()["status"];
                        setText(true);
                      });
                    }
                  : () {
                      print("table" + element.data()["tableNumber"]);
                      setState(() {
                        tableNumber = element.data()["tableNumber"];
                        status = element.data()["status"];
                      });
                    }
              : null,
          child: Container(
            height: double.parse(element.data()["length"]),
            width: double.parse(element.data()["width"]),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: colorConvert(element.data()["hexColor"]),
            ),
            child: Center(
                child: element.id != "entrance"
                    ? element.data()["status"] == "unavailable"
                        ? Icon(Icons.cancel_outlined,
                            size: 30, color: Colors.white)
                        : Text(element.data()["tableNumber"],
                            style: GoogleFonts.montserrat(
                                fontSize: 20,
                                fontWeight: FontWeight.w700,
                                color: Colors.white))
                    : Icon(Icons.sensor_door_outlined,
                        size: 25, color: Colors.white)),
          ),
        ),
      );
      tables.add(table);
    });
    return tables;
  }

  Widget legend(Size screenSize, bool iconBetter, IconData icon, Color color,
      String label) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4.0),
      child: Container(
        height: screenSize.height * 0.03,
        width: screenSize.width * 0.3,
        child: Row(
          children: [
            iconBetter == true
                ? CircleAvatar(
                    child: Icon(
                      icon,
                      color: Colors.black,
                    ),
                    backgroundColor: color,
                  )
                : CircleAvatar(
                    backgroundColor: color,
                  ),
            Text(label,
                style: GoogleFonts.openSans(
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff707070)))
          ],
        ),
      ),
    );
  }

  Widget table(
      Size screenSize, Color color, double height, double width, String label) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4.0),
      child: Container(
        width: screenSize.width * 0.5,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              height: height,
              width: width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color: color,
              ),
            ),
            Text(label,
                style: GoogleFonts.openSans(
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff707070)))
          ],
        ),
      ),
    );
  }
}

//table icon size
// width = width; height = length
// table for 1
// height: screenSize.height * 0.06,
// width: screenSize.width * 0.12,
// table for 2
// height: screenSize.height * 0.06,
// width: screenSize.width * 0.14,
// table for 4
// height: screenSize.height * 0.09,
// width: screenSize.width * 0.18,
// table for > 4
// height: screenSize.height * 0.13,
// width: screenSize.width * 0.18,
