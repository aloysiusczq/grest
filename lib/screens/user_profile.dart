import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:grest/models/user.dart';
import 'package:grest/providers/user_provider.dart';
import 'package:grest/screens/user_favourites.dart';
import 'package:image_picker/image_picker.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:provider/provider.dart';

class UserProfileScreen extends StatefulWidget {
  static String routeName = "user_profile_screen";
  @override
  State<StatefulWidget> createState() => _UserProfileScreen();
}

class _UserProfileScreen extends State<UserProfileScreen> {
  UserProvider userProvider;

  final _updateUserKey = new GlobalKey<FormState>();
  TextEditingController updateNameTEC = TextEditingController();
  TextEditingController updateNumberTEC = TextEditingController();
  Color borderSideColor = Color(0xffF8F8F8);

  @override
  Widget build(BuildContext context) {
    userProvider = Provider.of(context);
    Size screenSize = MediaQuery.of(context).size;
    return StreamBuilder<UserBody>(
        stream: userProvider.userData,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            UserBody currentUserProfile = snapshot.data;
            return Scaffold(
              backgroundColor: Color(0xffFFFFF0),
              body: CustomScrollView(
                  physics: NeverScrollableScrollPhysics(),
                  slivers: [
                    SliverPersistentHeader(
                      pinned: true,
                      floating: true,
                      delegate: CustomSliverDelegate(
                        currentUser: currentUserProfile,
                        expandedHeight: screenSize.height * 0.2,
                        screenSize: screenSize,
                        userProvider: userProvider,
                      ),
                    ),
                    SliverFillRemaining(
                      child: Padding(
                        padding: EdgeInsets.only(
                            top: 90.0,
                            right: screenSize.width / 9,
                            left: screenSize.width / 9),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Text("Email",
                                style: GoogleFonts.montserrat(
                                    fontSize: 25,
                                    fontWeight: FontWeight.w600,
                                    color: Color(0xff707070))),
                            Text(currentUserProfile.email,
                                style: GoogleFonts.raleway(
                                    fontSize: 17,
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xff707070))),
                            Text("Contact Number",
                                style: GoogleFonts.montserrat(
                                    fontSize: 25,
                                    fontWeight: FontWeight.w600,
                                    color: Color(0xff707070))),
                            Text(
                                currentUserProfile.contactNumber == ""
                                    ? "No contact number found"
                                    : currentUserProfile.contactNumber,
                                style: GoogleFonts.raleway(
                                    fontSize: 17,
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xff707070))),
                            InkWell(
                              onTap: () {
                                showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return SingleChildScrollView(
                                      child: AlertDialog(
                                        insetPadding:
                                            EdgeInsets.symmetric(vertical: 200),
                                        title: Text(
                                          "Update Details",
                                          style: GoogleFonts.montserrat(
                                              fontSize: 25,
                                              fontWeight: FontWeight.w600,
                                              color: Color(0xff707070)),
                                        ),
                                        content: Form(
                                          key: _updateUserKey,
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceAround,
                                            children: [
                                              Text("User's Name",
                                                  style: GoogleFonts.montserrat(
                                                      fontSize: 20,
                                                      fontWeight:
                                                          FontWeight.w300,
                                                      color:
                                                          Color(0xff707070))),
                                              textField(
                                                  hintText: "",
                                                  screenSize: screenSize,
                                                  icon: Icons
                                                      .supervised_user_circle,
                                                  textEditingController:
                                                      updateNameTEC),
                                              Text("Contact Number",
                                                  style: GoogleFonts.montserrat(
                                                      fontSize: 20,
                                                      fontWeight:
                                                          FontWeight.w300,
                                                      color:
                                                          Color(0xff707070))),
                                              textField(
                                                  textInputType: TextInputType
                                                      .numberWithOptions(),
                                                  hintText: "",
                                                  screenSize: screenSize,
                                                  icon: Icons
                                                      .phone_android_outlined,
                                                  textEditingController:
                                                      updateNumberTEC),
                                            ],
                                          ),
                                        ),
                                        actions: [
                                          FlatButton(
                                              onPressed: () {
                                                setState(() {
                                                  Navigator.of(context).pop();
                                                });
                                              },
                                              child: Text("Cancel")),
                                          FlatButton(
                                              onPressed: () {
                                                setState(() {
                                                  updateProfile(context);
                                                });
                                              },
                                              child: Text("Done"))
                                        ],
                                      ),
                                    );
                                  },
                                );
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10),
                                  boxShadow: [
                                    BoxShadow(
                                        blurRadius: 5, color: Color(0x3C000000))
                                  ],
                                ),
                                child: ListTile(
                                  leading: CircleAvatar(
                                      backgroundColor: Color(0xffF5F5DC),
                                      radius: 22.0,
                                      child: Icon(
                                        Icons.edit_outlined,
                                        size: 20,
                                        color: Colors.black,
                                      )),
                                  title: Text("Edit Profile",
                                      maxLines: 2,
                                      style: GoogleFonts.openSans(
                                          fontSize: 15,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xff707070))),
                                  trailing:
                                      Icon(Icons.arrow_forward_ios_outlined),
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                pushNewScreen(context, screen: UserFavScreen());
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10),
                                  boxShadow: [
                                    BoxShadow(
                                        blurRadius: 5, color: Color(0x3C000000))
                                  ],
                                ),
                                child: ListTile(
                                  leading: CircleAvatar(
                                      backgroundColor: Color(0xffF5F5DC),
                                      radius: 22.0,
                                      child: Icon(
                                        Icons.bookmark_border_outlined,
                                        size: 20,
                                        color: Colors.black,
                                      )),
                                  title: Text("My Favourites",
                                      maxLines: 2,
                                      style: GoogleFonts.openSans(
                                          fontSize: 15,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xff707070))),
                                  trailing:
                                      Icon(Icons.arrow_forward_ios_outlined),
                                ),
                              ),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                                boxShadow: [
                                  BoxShadow(
                                      blurRadius: 5, color: Color(0x3C000000))
                                ],
                              ),
                              child: ListTile(
                                leading: CircleAvatar(
                                    backgroundColor: Color(0xffF5F5DC),
                                    radius: 22.0,
                                    child: Icon(
                                      Icons.help_outline_outlined,
                                      size: 20,
                                      color: Colors.black,
                                    )),
                                title: Text("Help Center",
                                    maxLines: 2,
                                    style: GoogleFonts.openSans(
                                        fontSize: 15,
                                        fontWeight: FontWeight.w400,
                                        color: Color(0xff707070))),
                                trailing:
                                    Icon(Icons.arrow_forward_ios_outlined),
                              ),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                                boxShadow: [
                                  BoxShadow(
                                      blurRadius: 5, color: Color(0x3C000000))
                                ],
                              ),
                              child: ListTile(
                                leading: CircleAvatar(
                                    backgroundColor: Color(0xffF5F5DC),
                                    radius: 22.0,
                                    child: Icon(
                                      Icons.feedback_outlined,
                                      size: 20,
                                      color: Colors.black,
                                    )),
                                title: Text("Give feedback",
                                    maxLines: 2,
                                    style: GoogleFonts.openSans(
                                        fontSize: 15,
                                        fontWeight: FontWeight.w400,
                                        color: Color(0xff707070))),
                                trailing:
                                    Icon(Icons.arrow_forward_ios_outlined),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                userProvider.signOut(
                                    context, currentUserProfile.userName);
                              },
                              child: Container(
                                  height: screenSize.height * 0.06,
                                  width: screenSize.width * 0.8,
                                  decoration: BoxDecoration(
                                      border:
                                          Border.all(color: Color(0xff707070)),
                                      borderRadius: BorderRadius.circular(30.0),
                                      color: Color(0xff8788A8)),
                                  child: Center(
                                    child: Text("Log Out",
                                        style: GoogleFonts.montserrat(
                                            fontSize: 22,
                                            fontWeight: FontWeight.w300,
                                            color: Colors.white)),
                                  )),
                            )
                          ],
                        ),
                      ),
                    )
                  ]),
            );
          } else {
            return Container();
          }
        });
  }

  Widget textField({
    TextEditingController textEditingController,
    TextInputType textInputType,
    String hintText,
    Function onChanged,
    Function validator,
    Size screenSize,
    IconData icon,
  }) {
    return Container(
      color: Color(0xffF8F8F8),
      height: screenSize.height * 0.075,
      width: screenSize.width * 0.7,
      child: TextFormField(
        autofocus: false,
        textAlign: TextAlign.start,
        style:
            GoogleFonts.montserrat(fontSize: 15, fontWeight: FontWeight.w300),
        keyboardType: textInputType,
        controller: textEditingController,
        onChanged: onChanged,
        maxLines: 1,
        decoration: InputDecoration(
          prefixIcon: Icon(
            icon,
            color: Colors.grey,
          ),
          hintText: hintText,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            borderSide: BorderSide(color: borderSideColor),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            borderSide: BorderSide(color: borderSideColor),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            borderSide: BorderSide(color: borderSideColor),
          ),
        ),
        validator: validator,
      ),
    );
  }

  Future updateProfile(context) async {
    if (_updateUserKey.currentState.validate()) {
      await userProvider.updateUserProfile(
        userName: updateNameTEC.text,
        contactNumber: updateNumberTEC.text,
      );
      Navigator.pop(context);
    }
  }
}

class CustomSliverDelegate extends SliverPersistentHeaderDelegate {
  final UserBody currentUser;
  final double expandedHeight;
  final Size screenSize;
  final UserProvider userProvider;

  final _picker = ImagePicker();
  String encodedImageString;

  CustomSliverDelegate(
      {this.currentUser,
      @required this.expandedHeight,
      this.screenSize,
      this.userProvider});

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Stack(
      fit: StackFit.expand,
      overflow: Overflow.visible,
      children: [
        Container(
          padding: EdgeInsets.symmetric(vertical: screenSize.height * 0.1),
          width: screenSize.width,
          decoration: BoxDecoration(
              color: Color(0xffA8A887),
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(50),
                  bottomRight: Radius.circular(50))),
        ),
        Positioned(
          top: expandedHeight / 1.9 - shrinkOffset,
          left: screenSize.width / 9,
          child: Opacity(
              opacity: (1 - shrinkOffset / expandedHeight),
              child: Container(
                  height: screenSize.height * 0.2,
                  width: screenSize.width / 1.3,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Color(0xff707070),
                          blurRadius: 8,
                        ),
                      ],
                      borderRadius: BorderRadius.circular(30)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      InkWell(
                        onTap: () {
                          chooseProfilePic().then((value) {
                            print(value.path);
                            uploadFile(File(value.path)).then((value) {
                              userProvider.uploadProfilePicture(
                                  userId: currentUser.userId, imageUrl: value);
                            });
                          });
                        },
                        child: StreamBuilder(
                            stream: userProvider.userCollection
                                .doc(currentUser.userId)
                                .snapshots(),
                            builder: (context, snapshot) {
                              DocumentSnapshot documentSnapshot = snapshot.data;
                              if (!snapshot.hasData) {
                                return CircleAvatar(
                                    radius: 45,
                                    backgroundColor: Colors.grey[200],
                                    child: Icon(Icons.people_outline_outlined,
                                        color: Color(0xffA8A887)));
                              } else {
                                if (documentSnapshot.data()["profilePicUrl"] ==
                                    "") {
                                  return CircleAvatar(
                                      radius: 45,
                                      backgroundColor: Colors.grey[200],
                                      child: Icon(Icons.people_outline_outlined,
                                          color: Color(0xffA8A887)));
                                } else {
                                  return Container(
                                      width: 100.0,
                                      height: 100,
                                      decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: NetworkImage(
                                                  documentSnapshot
                                                      .data()["profilePicUrl"]),
                                              fit: BoxFit.cover),
                                          shape: BoxShape.circle));
                                }
                              }
                            }),
                      ),
                      Text(currentUser.userName,
                          style: GoogleFonts.lato(
                              fontSize: 28, fontWeight: FontWeight.w900))
                    ],
                  ))),
        ),
      ],
    );
  }

  Future chooseProfilePic() async {
    final pickedFile = await _picker.getImage(source: ImageSource.gallery);
    return pickedFile;
  }

  Future<String> uploadFile(File _image) async {
    FirebaseStorage storageReference = FirebaseStorage.instance;
    Reference ref =
        storageReference.ref().child("image" + DateTime.now().toString());
    TaskSnapshot taskSnapshot = await ref.putFile(_image);
    String returnURL;
    if (taskSnapshot.state == TaskState.success) {
      returnURL = await taskSnapshot.ref.getDownloadURL();
      print("returnURL $returnURL");
    }
    return returnURL;
  }

  @override
  double get maxExtent => expandedHeight;

  @override
  double get minExtent => kToolbarHeight;

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }
}
