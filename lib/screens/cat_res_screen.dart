import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:grest/models/restaurants.dart';
import 'package:grest/models/user.dart';
import 'package:grest/providers/restaurant_provider.dart';
import 'package:grest/screens/restaurant_details_screen.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:provider/provider.dart';

class CatsResScreen extends StatefulWidget {
  static String routeName = "cats_res_screen";
  final String categoryName;
  final UserBody currentUser;

  const CatsResScreen({Key key, this.categoryName, this.currentUser})
      : super(key: key);
  @override
  State<StatefulWidget> createState() => _CatsResScreen();
}

class _CatsResScreen extends State<CatsResScreen> {
  RestaurantProvider resProvider;
  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    resProvider = Provider.of(context);
    return Scaffold(
      backgroundColor: Color(0xffFFFFF0),
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(
              vertical: screenSize.height * 0.02,
              horizontal: screenSize.width * 0.05),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(widget.categoryName,
                  style: GoogleFonts.montserrat(
                      fontSize: 30, fontWeight: FontWeight.w700)),
              Divider(
                thickness: 1,
                color: Color(0xff8788A8),
              ),
              Expanded(
                child: Padding(
                    padding: EdgeInsets.only(top: screenSize.height * 0.02),
                    child: StreamBuilder(
                        stream: resProvider.resCollection
                            .where("restaurantCuisineTypes",
                                arrayContains: widget.categoryName)
                            .snapshots(),
                        builder: (context, snapshot) {
                          if (!snapshot.hasData) {
                            return Center(child: CircularProgressIndicator());
                          } else {
                            if (snapshot.data.docs.isEmpty) {
                              return Center(
                                  child: Text(
                                      "No restaurant available in this category"));
                            } else {
                              return ListView.builder(
                                  itemCount: snapshot.data.docs.length,
                                  itemBuilder: (context, index) {
                                    DocumentSnapshot documentSnapshot =
                                        snapshot.data.docs[index];
                                    return InkWell(
                                      onTap: () {
                                        pushNewScreen(
                                          context,
                                          screen: ResDetailScreen(
                                            currentUser: widget.currentUser,
                                            currentRestaurant:
                                                _resDataFromSnapShot(
                                                    documentSnapshot),
                                          ),
                                          withNavBar: false,
                                        );
                                      },
                                      child: Padding(
                                        padding:
                                            EdgeInsets.symmetric(vertical: 8.0),
                                        child: ListTile(
                                            leading: Container(
                                                width: 100.0,
                                                height: 100,
                                                decoration: BoxDecoration(
                                                    boxShadow: [
                                                      BoxShadow(
                                                          blurRadius: 5,
                                                          color:
                                                              Color(0x3C000000))
                                                    ],
                                                    image: DecorationImage(
                                                        image: NetworkImage(
                                                          documentSnapshot[
                                                              "restaurantPicUrl"],
                                                        ),
                                                        fit: BoxFit.cover),
                                                    shape: BoxShape.circle)),
                                            title: Text(
                                                documentSnapshot[
                                                    "restaurantName"],
                                                maxLines: 2,
                                                style: GoogleFonts.raleway(
                                                    fontSize: 15,
                                                    fontWeight: FontWeight.w400,
                                                    color: Color(0xff707070)))),
                                      ),
                                    );
                                  });
                            }
                          }
                        })),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Restaurant _resDataFromSnapShot(DocumentSnapshot snapshot) {
    return Restaurant(
      restaurantId: snapshot.reference.id,
      restaurantName: snapshot.data()["restaurantName"],
      restaurantAddress: snapshot.data()["restaurantAddress"],
      restaurantLocation: snapshot.data()["restaurantLocation"],
      restaurantOperateHrs: snapshot.data()["restaurantOperateHrs"],
      restaurantContactNumber: snapshot.data()["restaurantPriceRange"],
      restaurantCuisineTypes: snapshot.data()["restaurantCuisineTypes"],
      restaurantPriceRange: snapshot.data()["restaurantPriceRange"],
      restaurantLat: snapshot.data()["restaurantLat"] ?? "",
      restaurantLong: snapshot.data()["restaurantLong"] ?? "",
      restaurantPicUrl: snapshot.data()["restaurantPicUrl"] ?? "",
    );
  }
}
