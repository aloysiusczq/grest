import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:grest/providers/user_provider.dart';
import 'package:grest/screens/login_screen.dart';
import 'package:provider/provider.dart';

class RegisterScreen extends StatefulWidget {
  static String routeName = "register_screen";
  @override
  State<StatefulWidget> createState() => _RegisterScreen();
}

class _RegisterScreen extends State<RegisterScreen> {
  FirebaseAuth auth = FirebaseAuth.instance;
  UserProvider userProvider = UserProvider();

  final _registerformKey = new GlobalKey<FormState>();
  TextEditingController usernameTEC = TextEditingController();
  TextEditingController emailTEC = TextEditingController();
  TextEditingController passwordTEC = TextEditingController();
  TextEditingController cpasswordTEC = TextEditingController();
  Color borderSideColor = Color(0xffF8F8F8);

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    userProvider = Provider.of(context);
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            height: screenSize.height,
            width: screenSize.width,
            padding: EdgeInsets.symmetric(horizontal: screenSize.width * 0.1),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/rest_register.jpg"),
                    fit: BoxFit.cover)),
            child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(
                        bottom: screenSize.height * 0.07,
                        top: screenSize.height * 0.1),
                    child: Text("Register",
                        style: GoogleFonts.montserrat(
                            color: Colors.white,
                            fontSize: 50,
                            fontWeight: FontWeight.w600)),
                  ),
                  Container(
                    height: screenSize.height * 0.52,
                    width: screenSize.width * 0.8,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(50)),
                    child: Form(
                      key: _registerformKey,
                      child: Column(
                        children: [
                          Padding(
                            padding:
                                EdgeInsets.only(top: screenSize.height * 0.025),
                            child: Text("Join us Today",
                                style: GoogleFonts.montserrat(
                                    fontSize: 30,
                                    fontWeight: FontWeight.w600,
                                    color: Color(0xffA8A887))),
                          ),
                          Text("In ever-expanding community",
                              style: GoogleFonts.montserrat(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w300,
                                  color: Color(0xffA8A887))),
                          Padding(
                            padding:
                                EdgeInsets.only(top: screenSize.height * 0.02),
                            child: textField(
                                hintText: "Username",
                                screenSize: screenSize,
                                icon: Icons.account_circle_outlined,
                                textEditingController: usernameTEC,
                                validator: (val) {
                                  if (val.isEmpty) {
                                    return "Please do not leave it empty";
                                  }
                                }),
                          ),
                          Padding(
                            padding:
                                EdgeInsets.only(top: screenSize.height * 0.02),
                            child: textField(
                                hintText: "Email",
                                screenSize: screenSize,
                                icon: Icons.email_outlined,
                                textEditingController: emailTEC,
                                validator: (val) {
                                  if (val.isEmpty) {
                                    return "Please do not leave it empty";
                                  } else if (!val.contains("@")) {
                                    return "Please enter valid email format";
                                  }
                                }),
                          ),
                          Padding(
                            padding:
                                EdgeInsets.only(top: screenSize.height * 0.02),
                            child: textField(
                                hintText: "Password",
                                screenSize: screenSize,
                                icon: Icons.lock_outlined,
                                textEditingController: passwordTEC,
                                obscureText: true,
                                validator: (val) {
                                  if (val.isEmpty) {
                                    return "Please do not leave it empty";
                                  } else if (val.length < 6) {
                                    return "Password not strong enough";
                                  } else if (!RegExp(
                                          r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,}$')
                                      .hasMatch(val)) {
                                    return "At least one uppercase, one lowercase, \n and one number";
                                  }
                                }),
                          ),
                          Padding(
                            padding:
                                EdgeInsets.only(top: screenSize.height * 0.02),
                            child: textField(
                                hintText: "Confirm Password",
                                screenSize: screenSize,
                                icon: Icons.lock_outlined,
                                textEditingController: cpasswordTEC,
                                obscureText: true,
                                validator: (val) {
                                  if (val.isEmpty) {
                                    return "Please do not leave it empty";
                                  } else if (val != passwordTEC.text) {
                                    return "Password does not match";
                                  }
                                }),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: screenSize.height * 0.12),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        button(
                          screenSize,
                          "Sign in",
                          Color(0xffFFFFFF),
                          Color(0xffA887A6),
                          () {
                            toLoginScreen();
                          },
                        ),
                        button(
                          screenSize,
                          "Register",
                          Color(0xffA8A887),
                          Color(0xffFFFFFF),
                          () {
                            performRegister();
                          },
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget textField(
      {TextEditingController textEditingController,
      TextInputType textInputType,
      String hintText,
      bool obscureText = false,
      Function onChanged,
      Function validator,
      Size screenSize,
      IconData icon}) {
    return Container(
      color: Color(0xffF8F8F8),
      height: screenSize.height * 0.08,
      width: screenSize.width * 0.7,
      child: TextFormField(
        autofocus: false,
        textAlign: TextAlign.start,
        style:
            GoogleFonts.montserrat(fontSize: 15, fontWeight: FontWeight.w300),
        keyboardType: textInputType,
        controller: textEditingController,
        obscureText: obscureText,
        onChanged: onChanged,
        maxLines: 1,
        decoration: InputDecoration(
          prefixIcon: Icon(
            icon,
            color: Colors.grey,
          ),
          hintText: hintText,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            borderSide: BorderSide(color: borderSideColor),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            borderSide: BorderSide(color: borderSideColor),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            borderSide: BorderSide(color: borderSideColor),
          ),
        ),
        validator: validator,
      ),
    );
  }

  Widget button(Size screenSize, String text, Color buttonColor,
      Color textColor, void Function() onTap) {
    return InkWell(
      onTap: onTap,
      child: Container(
        height: screenSize.height * 0.08,
        width: screenSize.width * 0.37,
        decoration: BoxDecoration(
            color: buttonColor, borderRadius: BorderRadius.circular(15)),
        child: Center(
            child: Text(text,
                style: GoogleFonts.montserrat(
                    fontSize: 20,
                    fontWeight: FontWeight.w300,
                    color: textColor))),
      ),
    );
  }

  Future performRegister() async {
    if (_registerformKey.currentState.validate()) {
      dynamic registerResult = await userProvider.registerUser(
          context: context,
          userName: usernameTEC.text,
          email: emailTEC.text,
          password: passwordTEC.text);
      if (registerResult != null) {
        await showDialog(
            context: context,
            child: AlertDialog(
              title: Text("Dear User,",
                  style: GoogleFonts.montserrat(
                      fontSize: 22, fontWeight: FontWeight.w500)),
              content: Text("The account is successfully registered.",
                  style: GoogleFonts.montserrat(
                      fontSize: 18, fontWeight: FontWeight.w300)),
              actions: [
                FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text("Okay"))
              ],
            ));
        Navigator.of(context).pushNamed(LoginScreen.routeName);
      }
    }
  }

  toLoginScreen() {
    Navigator.of(context).pushNamed(LoginScreen.routeName);
  }
}
