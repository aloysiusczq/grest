import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:grest/providers/restaurant_provider.dart';
import 'package:grest/providers/user_provider.dart';
import 'package:grest/utils/globals.dart';
import 'package:provider/provider.dart';

class UserFavScreen extends StatefulWidget {
  static String routeName = "user_fav_screen";
  @override
  State<StatefulWidget> createState() => _UserFavScreen();
}

class _UserFavScreen extends State<UserFavScreen> {
  UserProvider userProvider;
  RestaurantProvider restaurantProvider;
  @override
  Widget build(BuildContext context) {
    userProvider = Provider.of(context);
    restaurantProvider = Provider.of(context);
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Color(0xffF5F5DC),
      body: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(vertical: screenSize.height * 0.1),
            width: screenSize.width,
            decoration: BoxDecoration(
                color: Color(0xffA8A887),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(50),
                    bottomRight: Radius.circular(50))),
            child: Center(
              child: Text("My Favourites",
                  style: GoogleFonts.montserrat(
                      fontWeight: FontWeight.w600,
                      fontSize: 30,
                      color: Colors.white)),
            ),
          ),
          Expanded(
            child: StreamBuilder(
                stream:
                    userProvider.userCollection.doc(currentUserId).snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return Center(child: CircularProgressIndicator());
                  } else {
                    if (snapshot.data.data()['favourites'].isEmpty) {
                      return Center(child: Text("No favourited restaurant"));
                    } else {
                      return ListView.builder(
                        itemCount: snapshot.data.data()['favourites'].length,
                        itemBuilder: (BuildContext context, int index) {
                          List favData = snapshot.data.data()['favourites'];
                          return Padding(
                            padding: EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 15.0),
                            child: Container(
                              height: screenSize.height * 0.15,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(30),
                                color: Colors.white,
                              ),
                              child: Center(
                                child: StreamBuilder(
                                    stream: restaurantProvider.resCollection
                                        .doc(favData[index])
                                        .snapshots(),
                                    builder: (context, snapshot) {
                                      DocumentSnapshot documentSnapshot =
                                          snapshot.data;
                                      if (!snapshot.hasData) {
                                        return Center(
                                            child: CircularProgressIndicator());
                                      } else {
                                        return ListTile(
                                          leading: Container(
                                            height: screenSize.height * 0.1,
                                            width: screenSize.width * 0.14,
                                            decoration: BoxDecoration(
                                              color: Colors.blue,
                                              borderRadius:
                                                  BorderRadius.circular(15),
                                              image: DecorationImage(
                                                image: NetworkImage(
                                                    documentSnapshot.data()[
                                                        "restaurantPicUrl"]),
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          ),
                                          title: Container(
                                            width: screenSize.width * 0.55,
                                            child: Text(
                                                documentSnapshot
                                                    .data()["restaurantName"],
                                                softWrap: true,
                                                maxLines: 2,
                                                style: GoogleFonts.montserrat(
                                                    fontSize: 25,
                                                    fontWeight: FontWeight.w500,
                                                    color: Color(0xff585847))),
                                          ),
                                          subtitle: Container(
                                            width: screenSize.width * 0.55,
                                            child: Text(
                                                documentSnapshot.data()[
                                                    "restaurantLocation"],
                                                softWrap: true,
                                                maxLines: 2,
                                                style: GoogleFonts.montserrat(
                                                    fontSize: 18,
                                                    fontWeight: FontWeight.w300,
                                                    color: Color(0xff707070))),
                                          ),
                                          trailing: InkWell(
                                            onTap: () {
                                              removeCurrentFav(favData[index]);
                                            },
                                            child: Icon(
                                              Icons
                                                  .remove_circle_outline_outlined,
                                              color: Colors.red,
                                            ),
                                          ),
                                        );
                                      }
                                    }),
                              ),
                            ),
                          );
                        },
                      );
                    }
                  }
                }),
          )
        ],
      ),
    );
  }

  Future removeCurrentFav(String restaurantId) async {
    dynamic result = userProvider.removeFavourite(
        userId: currentUserId, restaurantId: restaurantId, context: context);
    if (result == true) {
      print("The restaurant is deleted successfully");
    } else {}
  }
}
