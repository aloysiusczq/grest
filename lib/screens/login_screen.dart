import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:grest/providers/user_provider.dart';
import 'package:grest/screens/register_screen.dart';
import 'package:grest/widgets/bottom_nav_bar.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatefulWidget {
  static String routeName = "login_screen";

  final bool exiting;
  const LoginScreen({Key key, this.exiting}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _LoginScreen();
}

class _LoginScreen extends State<LoginScreen> {
  UserProvider userProvider;

  final _loginformKey = new GlobalKey<FormState>();
  TextEditingController emailTEC = TextEditingController();
  TextEditingController passwordTEC = TextEditingController();
  Color borderSideColor = Color(0xffF8F8F8);

  @override
  Widget build(BuildContext context) {
    userProvider = Provider.of(context);
    Size screenSize = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: () async {
        return widget.exiting == true
            // ignore: missing_return
            ? Future.value(false).then((value) {
                SystemNavigator.pop();
              })
            : Future.value(true);
      },
      child: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Scaffold(
          body: SingleChildScrollView(
            child: Container(
              height: screenSize.height,
              width: screenSize.width,
              padding: EdgeInsets.symmetric(horizontal: screenSize.width * 0.1),
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/images/cafe_login.jpg"),
                      fit: BoxFit.cover)),
              child: Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                          bottom: screenSize.height * 0.12,
                          top: screenSize.height * 0.1),
                      child: Text("Sign In",
                          style: GoogleFonts.montserrat(
                              color: Colors.white,
                              fontSize: 50,
                              fontWeight: FontWeight.w600)),
                    ),
                    Container(
                      height: screenSize.height * 0.4,
                      width: screenSize.width * 0.8,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(50)),
                      child: Form(
                        key: _loginformKey,
                        child: Column(
                          children: [
                            Padding(
                              padding: EdgeInsets.only(
                                  top: screenSize.height * 0.03),
                              child: Text("Welcome Back",
                                  style: GoogleFonts.montserrat(
                                      fontSize: 30,
                                      fontWeight: FontWeight.w600,
                                      color: Color(0xff6644CC))),
                            ),
                            Text("We love to see you again",
                                style: GoogleFonts.montserrat(
                                    fontSize: 15,
                                    fontWeight: FontWeight.w300,
                                    color: Color(0xff6644CC))),
                            Padding(
                              padding: EdgeInsets.only(
                                  top: screenSize.height * 0.05),
                              child: textField(
                                  hintText: "Email",
                                  screenSize: screenSize,
                                  icon: Icons.email_outlined,
                                  textEditingController: emailTEC,
                                  validator: (val) {
                                    if (val.isEmpty) {
                                      return "Please do not leave it empty";
                                    } else if (!val.contains("@")) {
                                      return "Please enter valid email format";
                                    }
                                  }),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  top: screenSize.height * 0.03),
                              child: textField(
                                  hintText: "Password",
                                  screenSize: screenSize,
                                  icon: Icons.lock_outlined,
                                  textEditingController: passwordTEC,
                                  obscureText: true,
                                  validator: (val) {
                                    if (val.isEmpty) {
                                      return "Please do not leave it empty";
                                    }
                                  }),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: screenSize.height * 0.12),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          button(
                            screenSize,
                            "Sign in",
                            Color(0xffA887A6),
                            Color(0xffFFFFFF),
                            () {
                              performLogin();
                            },
                          ),
                          button(
                            screenSize,
                            "Register",
                            Color(0xffFFFFFF),
                            Color(0xffA8A887),
                            () {
                              toRegisterScreen();
                            },
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget textField(
      {TextEditingController textEditingController,
      TextInputType textInputType,
      String hintText,
      bool obscureText = false,
      Function onChanged,
      Function validator,
      Size screenSize,
      IconData icon}) {
    return Container(
      color: Color(0xffF8F8F8),
      height: screenSize.height * 0.075,
      width: screenSize.width * 0.7,
      child: TextFormField(
        autofocus: false,
        textAlign: TextAlign.start,
        style:
            GoogleFonts.montserrat(fontSize: 15, fontWeight: FontWeight.w300),
        keyboardType: textInputType,
        controller: textEditingController,
        obscureText: obscureText,
        onChanged: onChanged,
        maxLines: 1,
        decoration: InputDecoration(
          prefixIcon: Icon(
            icon,
            color: Colors.grey,
          ),
          hintText: hintText,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            borderSide: BorderSide(color: borderSideColor),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            borderSide: BorderSide(color: borderSideColor),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            borderSide: BorderSide(color: borderSideColor),
          ),
        ),
        validator: validator,
      ),
    );
  }

  Widget button(Size screenSize, String text, Color buttonColor,
      Color textColor, void Function() onTap) {
    return InkWell(
      onTap: onTap,
      child: Container(
        height: screenSize.height * 0.08,
        width: screenSize.width * 0.37,
        decoration: BoxDecoration(
            color: buttonColor, borderRadius: BorderRadius.circular(15)),
        child: Center(
            child: Text(text,
                style: GoogleFonts.montserrat(
                    fontSize: 20,
                    fontWeight: FontWeight.w300,
                    color: textColor))),
      ),
    );
  }

  Future performLogin() async {
    if (_loginformKey.currentState.validate()) {
      dynamic loginResult = await userProvider.loginUser(
        context: context,
        email: emailTEC.text,
        password: passwordTEC.text,
      );
      Center(child: CircularProgressIndicator());
      if (loginResult != null) {
        Navigator.of(context).pushNamed(NavTab.routeName);
        //to home screen with nav bar
      }
    }
  }

  toRegisterScreen() {
    Navigator.of(context).pushNamed(RegisterScreen.routeName);
  }
}
