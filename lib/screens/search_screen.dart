import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:grest/models/restaurants.dart';
import 'package:grest/models/user.dart';
import 'package:grest/providers/restaurant_provider.dart';
import 'package:grest/screens/restaurant_details_screen.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:provider/provider.dart';

class SearchScreen extends StatefulWidget {
  static String routeName = "search_screen";
  final UserBody currentUser;

  const SearchScreen({Key key, this.currentUser}) : super(key: key);
  @override
  State<StatefulWidget> createState() => _SearchScreen();
}

class _SearchScreen extends State<SearchScreen> {
  RestaurantProvider resProvider;
  List<Restaurant> restaurantList;

  TextEditingController searchTEC = TextEditingController();
  Color borderSideColor = Colors.white;

  List filteredRes;

  @override
  void initState() {
    super.initState();
    filteredRes = [];
  }

  void filterRestaurants(value) {
    print("value $value");
    setState(() {
      filteredRes = restaurantList
          .where((element) => element.restaurantName
              .toLowerCase()
              .contains(value.toLowerCase()))
          .toList();
    });
    if (value == "") {
      setState(() {
        filteredRes = [];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    resProvider = Provider.of(context);
    return Scaffold(
      backgroundColor: Color(0xffFFFFF0),
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(
              vertical: screenSize.height * 0.02,
              horizontal: screenSize.width * 0.05),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("Search",
                  style: GoogleFonts.montserrat(
                      fontSize: 25, fontWeight: FontWeight.w700)),
              Padding(
                padding:
                    EdgeInsets.symmetric(vertical: screenSize.height * 0.03),
                child: Container(
                  height: screenSize.height * 0.06,
                  width: screenSize.width * 0.9,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(color: Color(0xffD3D3D3)),
                      borderRadius: BorderRadius.circular(15.0)),
                  child: Row(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(
                            left: screenSize.width * 0.04,
                            right: screenSize.width * 0.08),
                        child: Icon(Icons.search, color: Color(0xff6644CC)),
                      ),
                      Expanded(
                        child: textField(
                            hintText: "Search Restaurant",
                            textEditingController: searchTEC,
                            onChanged: (value) {
                              filterRestaurants(value);
                            }),
                      ),
                      InkWell(
                        onTap: () {
                          searchTEC.clear();
                          setState(() {
                            filteredRes = [];
                            FocusScope.of(context).unfocus();
                          });
                        },
                        child: Padding(
                          padding: EdgeInsets.only(
                              left: screenSize.width * 0.08,
                              right: screenSize.width * 0.04),
                          child: Icon(Icons.close, color: Color(0xff78849E)),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: EdgeInsets.only(bottom: screenSize.height * 0.03),
                    child: Container(
                      height: screenSize.height * 0.06,
                      width: screenSize.width * 0.42,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(color: Color(0xffD3D3D3)),
                          borderRadius: BorderRadius.circular(15.0)),
                      child: Row(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(
                                left: screenSize.width * 0.04,
                                right: screenSize.width * 0.06),
                            child: Icon(Icons.filter_list,
                                color: Color(0xff8F76A8)),
                          ),
                          Expanded(
                              child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    "Filter",
                                    style: GoogleFonts.montserrat(
                                        fontSize: 15,
                                        fontWeight: FontWeight.w300,
                                        color: Color(0xff78849E)),
                                  ))),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: screenSize.height * 0.03),
                    child: Container(
                      height: screenSize.height * 0.06,
                      width: screenSize.width * 0.42,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(color: Color(0xffD3D3D3)),
                          borderRadius: BorderRadius.circular(15.0)),
                      child: Row(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(
                                left: screenSize.width * 0.04,
                                right: screenSize.width * 0.06),
                            child: Icon(Icons.sort, color: Color(0xff8F76A8)),
                          ),
                          Expanded(
                              child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    "Sort",
                                    style: GoogleFonts.montserrat(
                                        fontSize: 15,
                                        fontWeight: FontWeight.w300,
                                        color: Color(0xff78849E)),
                                  ))),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Expanded(
                child: filteredRes.length > 0
                    ? Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: screenSize.height * 0.01),
                        child: GridView.count(
                            physics: ScrollPhysics(),
                            scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                            childAspectRatio:
                                (screenSize.width * 1.22 / screenSize.height),
                            crossAxisSpacing: screenSize.width * 0.07,
                            mainAxisSpacing: screenSize.height * 0.05,
                            crossAxisCount: 2,
                            children:
                                List.generate(filteredRes.length, (int index) {
                              return InkWell(
                                onTap: () {
                                  print(filteredRes[index].restaurantName);
                                  pushNewScreen(
                                    context,
                                    screen: ResDetailScreen(
                                        currentUser: widget.currentUser,
                                        currentRestaurant: filteredRes[index]),
                                    withNavBar: false,
                                  );
                                },
                                child: conCategory(
                                  screenSize,
                                  filteredRes[index].restaurantPicUrl,
                                  filteredRes[index].restaurantName,
                                  filteredRes[index].restaurantLocation,
                                ),
                              );
                            })),
                      )
                    : StreamBuilder(
                        stream: resProvider.listofRestauants,
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            restaurantList = snapshot.data;
                            return Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: screenSize.height * 0.01),
                              child: GridView.count(
                                  physics: ScrollPhysics(),
                                  scrollDirection: Axis.vertical,
                                  shrinkWrap: true,
                                  childAspectRatio: (screenSize.width *
                                      1.22 /
                                      screenSize.height),
                                  crossAxisSpacing: screenSize.width * 0.07,
                                  mainAxisSpacing: screenSize.height * 0.05,
                                  crossAxisCount: 2,
                                  children: List.generate(restaurantList.length,
                                      (int index) {
                                    return InkWell(
                                      onTap: () {
                                        print(restaurantList[index]
                                            .restaurantName);
                                        pushNewScreen(
                                          context,
                                          screen: ResDetailScreen(
                                              currentUser: widget.currentUser,
                                              currentRestaurant:
                                                  restaurantList[index]),
                                          withNavBar: false,
                                        );
                                      },
                                      child: conCategory(
                                        screenSize,
                                        restaurantList[index].restaurantPicUrl,
                                        restaurantList[index].restaurantName,
                                        restaurantList[index]
                                            .restaurantLocation,
                                      ),
                                    );
                                  })),
                            );
                          } else {
                            return Text("Finding restaurant enlisted T.T");
                          }
                        }),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget textField(
      {TextEditingController textEditingController,
      TextInputType textInputType,
      String hintText,
      Function onChanged,
      Function validator,
      Size screenSize}) {
    return TextFormField(
      autofocus: false,
      textAlign: TextAlign.start,
      style: GoogleFonts.montserrat(
          fontSize: 15, fontWeight: FontWeight.w300, color: Color(0xff78849E)),
      keyboardType: textInputType,
      controller: textEditingController,
      onChanged: onChanged,
      decoration: InputDecoration(
        hintText: hintText,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(5)),
          borderSide: BorderSide(color: borderSideColor),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(5)),
          borderSide: BorderSide(color: borderSideColor),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(5)),
          borderSide: BorderSide(color: borderSideColor),
        ),
      ),
      validator: validator,
    );
  }

  Widget conCategory(Size screenSize, String imageName, String restaurantName,
      String location) {
    return Container(
      width: screenSize.width * 0.5,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          border: Border.all(color: Color(0xffD3D3D3))),
      child: Column(
        children: [
          Container(
            height: screenSize.height * 0.2,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20.0),
                  topRight: Radius.circular(20.0),
                  bottomRight: Radius.circular(20.0)),
              color: Colors.grey,
              image: DecorationImage(
                  image: NetworkImage(imageName), fit: BoxFit.cover),
            ),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 5),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      restaurantName,
                      textAlign: TextAlign.start,
                      style: GoogleFonts.montserrat(
                          fontWeight: FontWeight.w500,
                          fontSize: 18.0,
                          color: Color(0xff707070)),
                    ),
                    Text(
                      location,
                      textAlign: TextAlign.start,
                      style: GoogleFonts.openSans(
                          fontSize: 13,
                          fontWeight: FontWeight.w400,
                          color: Color(0xff707070)),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
