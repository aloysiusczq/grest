import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:grest/models/restaurants.dart';
import 'package:grest/models/user.dart';
import 'package:grest/providers/restaurant_provider.dart';
import 'package:grest/widgets/bottom_nav_bar.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

class ResSumScreen extends StatefulWidget {
  static String routeName = "res_sum_screen";

  final String reserveDate;
  final String reserveTime;
  final String reserveAdult;
  final String reserveChild;
  final Restaurant reserveRes;
  final String tableNumber;
  final UserBody currentUser;

  const ResSumScreen(
      {Key key,
      this.reserveDate,
      this.reserveTime,
      this.reserveAdult,
      this.reserveChild,
      this.reserveRes,
      this.tableNumber,
      this.currentUser})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _ResSumScreen();
}

class _ResSumScreen extends State<ResSumScreen> {
  final _speReqKey = new GlobalKey<FormState>();
  TextEditingController speReqTEC = TextEditingController();
  Color borderSideColor = Color(0xffA8A887);

  RestaurantProvider resProvider;

  @override
  Widget build(BuildContext context) {
    resProvider = Provider.of(context);
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Color(0xffFFFFF0),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("Your Reservation",
                    style: GoogleFonts.montserrat(
                        fontSize: 25,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff707070))),
                Center(
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                        vertical: screenSize.height * 0.01),
                    child: Container(
                      height: screenSize.height * 0.18,
                      width: screenSize.width * 0.8,
                      decoration: BoxDecoration(
                        color: Colors.grey,
                        borderRadius: BorderRadius.circular(30.0),
                        image: DecorationImage(
                            image: NetworkImage(
                              widget.reserveRes.restaurantPicUrl,
                            ),
                            fit: BoxFit.cover),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 8.0, vertical: 10.0),
                            child: ListTile(
                              title: Text(
                                widget.reserveRes.restaurantName,
                                style: GoogleFonts.montserrat(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.white),
                              ),
                              subtitle: Text(
                                widget.reserveRes.restaurantLocation,
                                style: GoogleFonts.montserrat(
                                    fontSize: 15,
                                    fontWeight: FontWeight.w300,
                                    color: Colors.white),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                ListTile(
                    leading: Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        boxShadow: [
                          BoxShadow(blurRadius: 5, color: Color(0x3C000000))
                        ],
                      ),
                      child: CircleAvatar(
                          backgroundColor: Colors.white,
                          radius: 22.0,
                          child: Icon(
                            Icons.access_time_outlined,
                            size: 20,
                            color: Colors.black,
                          )),
                    ),
                    title: Text(widget.reserveDate + " " + widget.reserveTime,
                        maxLines: 2,
                        style: GoogleFonts.raleway(
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                            color: Color(0xff707070)))),
                ListTile(
                    leading: Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        boxShadow: [
                          BoxShadow(blurRadius: 5, color: Color(0x3C000000))
                        ],
                      ),
                      child: CircleAvatar(
                          backgroundColor: Colors.white,
                          radius: 22.0,
                          child: Icon(
                            Icons.people_outline,
                            size: 20,
                            color: Colors.black,
                          )),
                    ),
                    title: Text(
                        "${widget.reserveAdult} adults, ${widget.reserveChild} children",
                        maxLines: 2,
                        style: GoogleFonts.raleway(
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                            color: Color(0xff707070)))),
                ListTile(
                    leading: Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        boxShadow: [
                          BoxShadow(blurRadius: 5, color: Color(0x3C000000))
                        ],
                      ),
                      child: CircleAvatar(
                          backgroundColor: Colors.white,
                          radius: 22.0,
                          child: Icon(
                            Icons.book_online_outlined,
                            size: 20,
                            color: Colors.black,
                          )),
                    ),
                    title: Text("Table ${widget.tableNumber}",
                        maxLines: 2,
                        style: GoogleFonts.raleway(
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                            color: Color(0xff707070)))),
                Text("Customer Details",
                    style: GoogleFonts.montserrat(
                        fontSize: 25,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff707070))),
                ListTile(
                    leading: Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        boxShadow: [
                          BoxShadow(blurRadius: 5, color: Color(0x3C000000))
                        ],
                      ),
                      child: CircleAvatar(
                          backgroundColor: Colors.white,
                          radius: 22.0,
                          child: Icon(
                            Icons.person_outline,
                            size: 20,
                            color: Colors.black,
                          )),
                    ),
                    title: Text(widget.currentUser.userName,
                        maxLines: 2,
                        style: GoogleFonts.raleway(
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                            color: Color(0xff707070)))),
                ListTile(
                    leading: Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        boxShadow: [
                          BoxShadow(blurRadius: 5, color: Color(0x3C000000))
                        ],
                      ),
                      child: CircleAvatar(
                          backgroundColor: Colors.white,
                          radius: 22.0,
                          child: Icon(
                            Icons.phone,
                            size: 20,
                            color: Colors.black,
                          )),
                    ),
                    title: Text("+6 ${widget.currentUser.contactNumber}",
                        maxLines: 2,
                        style: GoogleFonts.raleway(
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                            color: Color(0xff707070)))),
                Text("Special Request",
                    style: GoogleFonts.montserrat(
                        fontSize: 25,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff707070))),
                ListTile(
                  leading: Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      boxShadow: [
                        BoxShadow(blurRadius: 5, color: Color(0x3C000000))
                      ],
                    ),
                    child: CircleAvatar(
                        backgroundColor: Colors.white,
                        radius: 22.0,
                        child: Icon(
                          Icons.note,
                          size: 20,
                          color: Colors.black,
                        )),
                  ),
                  title: Form(
                    key: _speReqKey,
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: screenSize.height * 0.01),
                      child: textField(
                          hintText: "",
                          screenSize: screenSize,
                          textEditingController: speReqTEC),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(right: 8.0, bottom: 8.0, left: 8.0),
                  child: Text(
                      "By confirming the reservation, you agreed to the Terms and Conditions provided by Grest and respective restaurant.",
                      maxLines: 2,
                      style: GoogleFonts.raleway(
                          fontSize: 15,
                          fontWeight: FontWeight.w300,
                          color: Color(0xff707070))),
                ),
                InkWell(
                  onTap: () {
                    updateTableStatus();
                  },
                  child: Center(
                    child: Container(
                        height: screenSize.height * 0.07,
                        width: screenSize.width * 0.8,
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(25.0),
                            color: Color(0xff8788A8)),
                        child: Center(
                          child: Text("Confirm Reservation",
                              style: GoogleFonts.montserrat(
                                  fontSize: 22,
                                  fontWeight: FontWeight.w300,
                                  color: Colors.white)),
                        )),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget textField(
      {TextEditingController textEditingController,
      TextInputType textInputType,
      String hintText,
      Function onChanged,
      Function validator,
      Size screenSize}) {
    return Container(
      color: Color(0xffF8F8F8),
      width: screenSize.width,
      child: TextFormField(
        autofocus: false,
        textAlign: TextAlign.start,
        style:
            GoogleFonts.montserrat(fontSize: 15, fontWeight: FontWeight.w300),
        keyboardType: textInputType,
        controller: textEditingController,
        onChanged: onChanged,
        maxLines: 3,
        decoration: InputDecoration(
          hintText: hintText,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(15)),
            borderSide: BorderSide(color: borderSideColor),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(15)),
            borderSide: BorderSide(color: borderSideColor),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(15)),
            borderSide: BorderSide(color: borderSideColor),
          ),
        ),
        validator: validator,
      ),
    );
  }

  Future updateTableStatus() async {
    dynamic result = await resProvider.updateTableStatus(
        context: context,
        currentId: widget.reserveRes.restaurantId,
        tableId: "table" + widget.tableNumber);
    if (result == true) {
      print("result $result");
      resProvider
          .createReservation(
              context: context,
              currentUserId: widget.currentUser.userId,
              currentUserName: widget.currentUser.userName,
              currentResId: widget.reserveRes.restaurantId,
              tableNumber: widget.tableNumber,
              specialRequest:
                  speReqTEC.text == "" ? "No special request" : speReqTEC.text,
              adultNumber: widget.reserveAdult,
              childrenNumber: widget.reserveChild,
              submissiondt:
                  DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now()))
          .then((value) => Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (BuildContext context) {
                  return NavTab();
                }),
                ModalRoute.withName('/'),
              ));
    }
  }
}
