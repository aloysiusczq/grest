import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:grest/models/restaurants.dart';
import 'package:grest/models/user.dart';
import 'package:grest/screens/new_reservation_table_screen.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:intl/intl.dart';

class NewResScreen extends StatefulWidget {
  static String routeName = "new_res_screen";
  final Restaurant selectedRes;
  final UserBody currentUser;

  const NewResScreen({Key key, this.selectedRes, this.currentUser})
      : super(key: key);
  @override
  State<StatefulWidget> createState() => _NewResScreen();
}

class _NewResScreen extends State<NewResScreen>
    with SingleTickerProviderStateMixin {
  TabController _mainTabController;
  List<String> mainTabTitle = ["Date", "Time", "Crowd"];

  CalendarController _calendarController = CalendarController();
  TextEditingController _dateTEC = TextEditingController();
  DateTime selectedDate;

  TextEditingController _timeTEC = TextEditingController();

  TextEditingController _adultTEC = TextEditingController();
  TextEditingController _childTEC = TextEditingController();
  Color borderSideColor = Color(0xff707070);

  @override
  void initState() {
    _mainTabController =
        new TabController(length: mainTabTitle.length, vsync: this);
    _mainTabController
      ..addListener(() {
        if (_mainTabController.indexIsChanging) {
          setState(() {});
        }
      });
    _dateTEC.text = _onDaySelected(DateTime.now());
    _timeTEC.text = _onTimeSelected(DateTime.now());

    super.initState();
  }

  @override
  dispose() {
    _mainTabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Color(0xffFFFFF0),
      body: SafeArea(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(
              vertical: screenSize.height * 0.02,
              horizontal: screenSize.width * 0.05,
            ),
            child: Text("Reservation for",
                style: GoogleFonts.montserrat(
                    fontSize: 30,
                    fontWeight: FontWeight.w500,
                    color: Color(0xff707070))),
          ),
          Center(
            child: Padding(
              padding: EdgeInsets.only(
                  bottom: _mainTabController.index != 0
                      ? screenSize.height * 0.02
                      : screenSize.height * 0.04),
              child: Container(
                width: screenSize.width * 0.8,
                decoration: BoxDecoration(
                  color: Colors.grey,
                  borderRadius: BorderRadius.circular(30.0),
                  image: DecorationImage(
                      image: AssetImage("assets/images/cafe_cat.png"),
                      fit: BoxFit.cover,
                      colorFilter: ColorFilter.mode(
                          Color(0x66000000), BlendMode.dstATop)),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 8.0, vertical: 10.0),
                      child: ListTile(
                        title: Text(
                          widget.selectedRes.restaurantName,
                          style: GoogleFonts.montserrat(
                              fontSize: 30,
                              fontWeight: FontWeight.w500,
                              color: Colors.white),
                        ),
                        subtitle: Text(
                          widget.selectedRes.restaurantLocation,
                          style: GoogleFonts.montserrat(
                              fontSize: 20,
                              fontWeight: FontWeight.w300,
                              color: Colors.white),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          _mainTabController.index != 0
              ? _mainTabController.index == 1 && _dateTEC.text != null
                  ? Center(
                      child: Padding(
                        padding:
                            EdgeInsets.only(bottom: screenSize.height * 0.02),
                        child: Container(
                          height: screenSize.height * 0.05,
                          width: screenSize.width * 0.3,
                          decoration: BoxDecoration(
                            border: Border.all(color: Color(0xffA8A887)),
                            borderRadius: BorderRadius.circular(50.0),
                            color: Color(0xffFFFFBF),
                          ),
                          child: Center(
                            child: Text(_dateTEC.text,
                                style: GoogleFonts.montserrat(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.black)),
                          ),
                        ),
                      ),
                    )
                  : _mainTabController.index == 2 &&
                          (_dateTEC.text != null && _timeTEC.text != null)
                      ? Center(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Padding(
                                padding: EdgeInsets.only(
                                    bottom: screenSize.height * 0.02),
                                child: Container(
                                  height: screenSize.height * 0.05,
                                  width: screenSize.width * 0.3,
                                  decoration: BoxDecoration(
                                    border:
                                        Border.all(color: Color(0xffA8A887)),
                                    borderRadius: BorderRadius.circular(50.0),
                                    color: Color(0xffFFFFBF),
                                  ),
                                  child: Center(
                                    child: Text(_dateTEC.text,
                                        style: GoogleFonts.montserrat(
                                            fontSize: 20,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.black)),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                    bottom: screenSize.height * 0.02),
                                child: Container(
                                  height: screenSize.height * 0.05,
                                  width: screenSize.width * 0.3,
                                  decoration: BoxDecoration(
                                    border:
                                        Border.all(color: Color(0xffA8A887)),
                                    borderRadius: BorderRadius.circular(50.0),
                                    color: Color(0xffFFFFBF),
                                  ),
                                  child: Center(
                                    child: Text(_timeTEC.text,
                                        style: GoogleFonts.montserrat(
                                            fontSize: 20,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.black)),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      : Container()
              : Container(),
          Expanded(
              child: Container(
            width: screenSize.width,
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: Color(0x5F707070)),
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25.0),
                topRight: Radius.circular(25.0),
              ),
            ),
            child: Column(
              children: [
                TabBar(
                  controller: _mainTabController,
                  isScrollable: true,
                  indicator: UnderlineTabIndicator(
                    borderSide:
                        BorderSide(color: Color(0xffA887A6), width: 3.0),
                  ),
                  tabs: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 25.0),
                      child: Tab(
                        child: Text(mainTabTitle[0],
                            style: GoogleFonts.montserrat(
                                fontSize:
                                    _mainTabController.index == 0 ? 18 : 15,
                                fontWeight: _mainTabController.index == 0
                                    ? FontWeight.w700
                                    : FontWeight.w400,
                                color: _mainTabController.index == 0
                                    ? Color(0xffA887A6)
                                    : Color(0xffD3D3D3))),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 25.0),
                      child: Tab(
                        child: Text(mainTabTitle[1],
                            style: GoogleFonts.montserrat(
                                fontSize:
                                    _mainTabController.index == 1 ? 18 : 15,
                                fontWeight: _mainTabController.index == 1
                                    ? FontWeight.w700
                                    : FontWeight.w400,
                                color: _mainTabController.index == 1
                                    ? Color(0xffA887A6)
                                    : Color(0xffD3D3D3))),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 25.0),
                      child: Tab(
                        child: Text(mainTabTitle[2],
                            style: GoogleFonts.montserrat(
                                fontSize:
                                    _mainTabController.index == 2 ? 18 : 15,
                                fontWeight: _mainTabController.index == 2
                                    ? FontWeight.w700
                                    : FontWeight.w400,
                                color: _mainTabController.index == 2
                                    ? Color(0xffA887A6)
                                    : Color(0xffD3D3D3))),
                      ),
                    ),
                  ],
                ),
                Expanded(
                  child: TabBarView(controller: _mainTabController, children: [
                    selectDate(screenSize),
                    selectTime(screenSize),
                    selectCrowd(screenSize),
                  ]),
                )
              ],
            ),
          )),
        ],
      )),
    );
  }

  Widget selectDate(Size screenSize) {
    return SingleChildScrollView(
      child: Column(
        children: [
          TableCalendar(
            calendarController: _calendarController,
            availableGestures: AvailableGestures.horizontalSwipe,
            startingDayOfWeek: StartingDayOfWeek.sunday,
            availableCalendarFormats: const {
              CalendarFormat.month: '',
            },
            calendarStyle: CalendarStyle(
              selectedColor: Colors.blue[400],
              todayColor: Colors.deepOrange[200],
              markersColor: Colors.brown[700],
              outsideDaysVisible: false,
            ),
            headerStyle: HeaderStyle(
              formatButtonTextStyle:
                  TextStyle().copyWith(color: Colors.white, fontSize: 15.0),
              formatButtonDecoration: BoxDecoration(
                color: Colors.deepOrange[400],
                borderRadius: BorderRadius.circular(16.0),
              ),
            ),
            onDaySelected: (date, events, _) {
              setState(() {
                _dateTEC.text = _onDaySelected(date);
                selectedDate = date;
              });
            },
          ),
          Container(
              height: screenSize.height * 0.08,
              width: screenSize.width * 0.7,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                border: Border.all(color: Color(0xff707070)),
              ),
              child: Center(
                  child: Text(_dateTEC.text,
                      style: GoogleFonts.montserrat(
                          fontSize: 22, fontWeight: FontWeight.w500)))),
          Padding(
            padding: EdgeInsets.symmetric(vertical: screenSize.height * 0.02),
            child: InkWell(
              onTap: () {
                if (calculateDateDifference(selectedDate) < 0) {
                  showDialog(
                      context: context,
                      child: AlertDialog(
                        title: Text("Dear user",
                            style: GoogleFonts.montserrat(
                                fontSize: 22, fontWeight: FontWeight.w500)),
                        content: Text("The selected date is not available",
                            style: GoogleFonts.montserrat(
                                fontSize: 18, fontWeight: FontWeight.w300)),
                        actions: [
                          FlatButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: Text("Try again"))
                        ],
                      ));
                } else {
                  _mainTabController..animateTo((_mainTabController.index + 1));
                }
              },
              child: Container(
                  height: screenSize.height * 0.07,
                  width: screenSize.width * 0.8,
                  decoration: BoxDecoration(
                      border: Border.all(color: Color(0xff707070)),
                      borderRadius: BorderRadius.circular(20.0),
                      color: Color(0xff8788A8)),
                  child: Center(
                    child: Text("Next",
                        style: GoogleFonts.montserrat(
                            fontSize: 22,
                            fontWeight: FontWeight.w300,
                            color: Colors.white)),
                  )),
            ),
          )
        ],
      ),
    );
  }

  _onDaySelected(DateTime date) {
    return DateFormat("yyyy-MM-dd").format(date).toString();
  }

  int calculateDateDifference(DateTime date) {
    DateTime now = DateTime.now();
    if (date != null) {
      return DateTime(date.year, date.month, date.day)
          .difference(DateTime(now.year, now.month, now.day))
          .inDays;
    } else {
      return 0;
    }
  }

  Widget selectTime(Size screenSize) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.all(8.0),
          child: Container(
            child: Text(
                "Please select a time, if the time shown below is unsuitable",
                maxLines: 2,
                textAlign: TextAlign.center,
                style: GoogleFonts.montserrat(
                    fontSize: 22,
                    fontWeight: FontWeight.w300,
                    color: Color(0xff707070))),
          ),
        ),
        Expanded(
          child: InkWell(
            onTap: () {
              showTimePicker(
                  context: context,
                  initialTime: TimeOfDay.now(),
                  builder: (context, child) {
                    return Theme(
                      data: Theme.of(context).copyWith(
                        primaryColor: Colors.green,
                        buttonTheme: ButtonTheme.of(context).copyWith(
                          colorScheme: ColorScheme.light(
                            secondary: Colors.yellow,
                          ),
                        ),
                      ),
                      child: child,
                    );
                  }).then((time) => setState(() {
                    if (time != null) {
                      final MaterialLocalizations localizations =
                          MaterialLocalizations.of(context);
                      final String formattedTimeOfDay = localizations
                          .formatTimeOfDay(time, alwaysUse24HourFormat: true);
                      print(formattedTimeOfDay);
                      _timeTEC.text = formattedTimeOfDay;
                    }
                  }));
            },
            child: Center(
              child: Container(
                child: Text("Choose time",
                    maxLines: 2,
                    textAlign: TextAlign.center,
                    style: GoogleFonts.montserrat(
                        fontSize: 25,
                        fontWeight: FontWeight.w600,
                        color: Color(0xffA8A887))),
              ),
            ),
          ),
        ),
        Container(
            height: screenSize.height * 0.08,
            width: screenSize.width * 0.7,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              border: Border.all(color: Color(0xff707070)),
            ),
            child: Center(
                child: Text(_timeTEC.text,
                    style: GoogleFonts.montserrat(
                        fontSize: 22, fontWeight: FontWeight.w500)))),
        Padding(
          padding: EdgeInsets.symmetric(vertical: screenSize.height * 0.02),
          child: InkWell(
            onTap: () {
              DateTime tempTime = DateFormat('HH:mm').parse(_timeTEC.text);
              if (calculateTimeDifference(tempTime) < 0) {
                showDialog(
                    context: context,
                    child: AlertDialog(
                      title: Text("Dear user",
                          style: GoogleFonts.montserrat(
                              fontSize: 22, fontWeight: FontWeight.w500)),
                      content: Text("The selected time is not available",
                          style: GoogleFonts.montserrat(
                              fontSize: 18, fontWeight: FontWeight.w300)),
                      actions: [
                        FlatButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: Text("Try again"))
                      ],
                    ));
              } else {
                _mainTabController..animateTo((_mainTabController.index + 1));
              }
            },
            child: Container(
                height: screenSize.height * 0.07,
                width: screenSize.width * 0.8,
                decoration: BoxDecoration(
                    border: Border.all(color: Color(0xff707070)),
                    borderRadius: BorderRadius.circular(20.0),
                    color: Color(0xff8788A8)),
                child: Center(
                  child: Text("Next",
                      style: GoogleFonts.montserrat(
                          fontSize: 22,
                          fontWeight: FontWeight.w300,
                          color: Colors.white)),
                )),
          ),
        )
      ],
    );
  }

  _onTimeSelected(DateTime time) {
    return DateFormat('HH:mm').format(DateTime.now().add(Duration(hours: 1)));
  }

  int calculateTimeDifference(DateTime time) {
    DateTime now = DateTime.now();
    if (time != null) {
      return DateTime(time.hour, time.minute)
          .difference(DateTime(now.hour, now.minute))
          .inHours;
    } else {
      return 0;
    }
  }

  Widget selectCrowd(Size screenSize) {
    return Column(
      children: [
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: EdgeInsets.all(20.0),
                  child: Text("Table for",
                      maxLines: 2,
                      style: GoogleFonts.montserrat(
                          fontSize: 28,
                          fontWeight: FontWeight.w600,
                          color: Color(0xff707070))),
                ),
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                          padding: EdgeInsets.only(left: 20.0, bottom: 20.0),
                          child: Text("Adults",
                              maxLines: 2,
                              style: GoogleFonts.montserrat(
                                  fontSize: 22,
                                  fontWeight: FontWeight.w300,
                                  color: Color(0xff707070))),
                        ),
                      ),
                      textField(
                          textEditingController: _adultTEC,
                          textInputType: TextInputType.number,
                          screenSize: screenSize,
                          textInputAction: TextInputAction.next),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                          padding: EdgeInsets.all(20.0),
                          child: Text("Children",
                              maxLines: 2,
                              style: GoogleFonts.montserrat(
                                  fontSize: 22,
                                  fontWeight: FontWeight.w300,
                                  color: Color(0xff707070))),
                        ),
                      ),
                      textField(
                          textEditingController: _childTEC,
                          textInputType: TextInputType.number,
                          screenSize: screenSize,
                          textInputAction: TextInputAction.done),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: screenSize.height * 0.01),
          child: InkWell(
            onTap: () {
              if (_adultTEC.text == "" || _childTEC.text == "") {
                showDialog(
                    context: context,
                    child: AlertDialog(
                      title: Text("Dear user",
                          style: GoogleFonts.montserrat(
                              fontSize: 22, fontWeight: FontWeight.w500)),
                      content: Text("Please do not leave any field empty.",
                          style: GoogleFonts.montserrat(
                              fontSize: 18, fontWeight: FontWeight.w300)),
                      actions: [
                        FlatButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: Text("Okay"))
                      ],
                    ));
              } else {
                print(int.parse(_adultTEC.text) + int.parse(_childTEC.text));
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return NewResTableScreen(
                    reserveDate: _dateTEC.text,
                    reserveTime: _timeTEC.text,
                    reserveAdult: _adultTEC.text,
                    reserveChild: _childTEC.text,
                    reserveRes: widget.selectedRes,
                    currentUser: widget.currentUser,
                  );
                }));
              }
              // Navigator.of(context).pushNamed(NewResTableScreen.routeName);
            },
            child: Container(
                height: screenSize.height * 0.07,
                width: screenSize.width * 0.8,
                decoration: BoxDecoration(
                    border: Border.all(color: Color(0xff707070)),
                    borderRadius: BorderRadius.circular(20.0),
                    color: Color(0xff8788A8)),
                child: Center(
                  child: Text("Next",
                      style: GoogleFonts.montserrat(
                          fontSize: 22,
                          fontWeight: FontWeight.w300,
                          color: Colors.white)),
                )),
          ),
        )
      ],
    );
  }

  Widget textField(
      {TextEditingController textEditingController,
      TextInputType textInputType,
      TextInputAction textInputAction,
      String hintText,
      Function onChanged,
      Function validator,
      Size screenSize}) {
    return Container(
      width: screenSize.width * 0.7,
      color: Color(0xffFFFFFF),
      child: TextFormField(
        textInputAction: textInputAction,
        autofocus: false,
        textAlign: TextAlign.center,
        style:
            GoogleFonts.montserrat(fontSize: 20, fontWeight: FontWeight.w700),
        keyboardType: textInputType,
        controller: textEditingController,
        onChanged: onChanged,
        maxLines: 1,
        decoration: InputDecoration(
          hintText: hintText,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(20)),
            borderSide: BorderSide(color: borderSideColor),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(20)),
            borderSide: BorderSide(color: borderSideColor),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(20)),
            borderSide: BorderSide(color: borderSideColor),
          ),
        ),
        validator: validator,
      ),
    );
  }
}
