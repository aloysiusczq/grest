import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:grest/screens/login_screen.dart';
import 'package:grest/widgets/swiper_pagination.dart';

class LandingScreen extends StatefulWidget {
  static String routeName = "landing_screen";
  @override
  State<StatefulWidget> createState() => _LandingScreen();
}

class _LandingScreen extends State<LandingScreen> {
  final SwiperController _swiperController = SwiperController();
  final int _pageCount = 3;
  int _currentIndex = 0;
  final List<String> images = [
    "assets/images/cafe_cat.png",
    "assets/images/rest_register.jpg",
    "assets/images/casual_cat.jpg",
  ];

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: screenSize.height * 0.03),
            child: Container(
              height: screenSize.height * 0.18,
              width: screenSize.width * 0.4,
              child: Image(
                image: AssetImage("assets/images/logo.png"),
                fit: BoxFit.fitHeight,
              ),
            ),
          ),
          Expanded(
              child: Swiper(
            index: _currentIndex,
            controller: _swiperController,
            itemCount: _pageCount,
            onIndexChanged: (index) {
              setState(() {
                _currentIndex = index;
              });
            },
            autoplay: true,
            // loop: false,
            itemBuilder: (context, index) {
              return _buildPage(images: images[index]);
            },
            pagination: SwiperPagination(
                builder: CustomPaginationBuilder(
                    activeSize: Size(8.0, 35.0),
                    activeColor: Colors.black,
                    size: Size(8.0, 20.0),
                    color: Color(0xff707070))),
          )),
          SizedBox(height: screenSize.height * 0.01),
          _buildButtons(),
        ],
      ),
    );
  }

  Widget _buildButtons() {
    return Container(
      margin: const EdgeInsets.only(right: 16.0, bottom: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          FlatButton(
            child: Text("Skip",
                style: GoogleFonts.montserrat(
                  fontSize: 22,
                  fontWeight: FontWeight.w300,
                  color: Color(0xff707070),
                )),
            onPressed: () {
              Navigator.of(context).pushNamed(LoginScreen.routeName);
            },
          ),
          IconButton(
            icon: Icon(
              _currentIndex < _pageCount - 1
                  ? Icons.arrow_forward_ios
                  : Icons.check_circle_outline,
              size: 40,
            ),
            onPressed: () async {
              if (_currentIndex < _pageCount - 1)
                _swiperController.next();
              else {
                Navigator.of(context).pushNamed(LoginScreen.routeName);
              }
            },
          )
        ],
      ),
    );
  }

  Widget _buildPage({String images}) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.only(right: 55.0, bottom: 55.0, left: 55.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30.0),
          color: Colors.grey,
          image: DecorationImage(image: AssetImage(images), fit: BoxFit.cover),
          boxShadow: [
            BoxShadow(
                blurRadius: 10.0,
                spreadRadius: 5.0,
                offset: Offset(5.0, 5.0),
                color: Colors.black26)
          ]),
    );
  }
}
