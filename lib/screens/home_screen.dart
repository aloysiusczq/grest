import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:grest/models/restaurants.dart';
import 'package:grest/models/user.dart';
import 'package:grest/providers/restaurant_provider.dart';
import 'package:grest/providers/user_provider.dart';
import 'package:grest/screens/all_categories_screen.dart';
import 'package:grest/screens/cat_res_screen.dart';
import 'package:grest/screens/restaurant_details_screen.dart';
import 'package:grest/screens/search_screen.dart';
import 'package:grest/utils/globals.dart';
import 'package:grest/widgets/swiper_pagination.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  static String routeName = "home_screen";
  final PersistentTabController controller;

  const HomeScreen({Key key, this.controller}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _HomeScreen();
}

class _HomeScreen extends State<HomeScreen> {
  final SwiperController _swiperController = SwiperController();
  final int _pageCount = 3;
  int _currentIndex = 0;
  final List<String> images = [
    "assets/images/casual_cat.jpg",
    "assets/images/cafe_cat.png",
    "assets/images/fine_din_cat.jpg",
    "assets/images/fine_din_cat.jpg",
  ];

  UserProvider userProvider;
  RestaurantProvider resProvider;
  List<Restaurant> restaurantList = [];

  @override
  void initState() {
    super.initState();
    print("here: ${widget.controller.index}");
  }

  @override
  Widget build(BuildContext context) {
    userProvider = Provider.of(context);
    resProvider = Provider.of(context);
    Size screenSize = MediaQuery.of(context).size;
    return StreamBuilder<UserBody>(
        stream: userProvider.userData,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            UserBody currentUser = snapshot.data;
            return Scaffold(
                backgroundColor: Color(0xffFFFFF0),
                body: SingleChildScrollView(
                  child: SafeArea(
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(
                              top: screenSize.height * 0.01,
                              right: screenSize.width * 0.04,
                              bottom: screenSize.height * 0.02,
                              left: screenSize.width * 0.04),
                          child: InkWell(
                            onTap: () {
                              widget.controller.jumpToTab(2);
                              // pushNewScreen(context, screen: UserProfileScreen());
                            },
                            child: ListTile(
                              leading: currentUser.userProfilePic != ""
                                  ? Container(
                                      width: 100.0,
                                      height: 100,
                                      decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: NetworkImage(
                                                  currentUser.userProfilePic),
                                              fit: BoxFit.cover),
                                          shape: BoxShape.circle))
                                  : CircleAvatar(
                                      backgroundColor: Colors.grey[200],
                                      child: Icon(Icons.people_outline_outlined,
                                          color: Color(0xffA8A887))),
                              title: Text("HELLO",
                                  style: GoogleFonts.openSans(
                                      fontSize: 15,
                                      fontWeight: FontWeight.w300)),
                              subtitle: Text(currentUser.userName,
                                  style: GoogleFonts.raleway(
                                      fontSize: 25,
                                      fontWeight: FontWeight.w700,
                                      color: Colors.black)),
                              trailing: Icon(Icons.arrow_forward_ios_outlined),
                            ),
                          ),
                        ),
                        Text(
                          "Where would you like to reserve your table ?",
                          style: GoogleFonts.montserrat(
                              fontSize: 18,
                              fontWeight: FontWeight.w700,
                              color: Color(0xff707070)),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: screenSize.height * 0.03),
                          child: Container(
                            height: screenSize.height * 0.06,
                            width: screenSize.width * 0.9,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                border: Border.all(color: Color(0xffA887A6)),
                                borderRadius: BorderRadius.circular(15.0),
                                boxShadow: [
                                  BoxShadow(
                                    color: Color(0xffA887A6),
                                    blurRadius: 8,
                                  ),
                                ]),
                            child: Row(
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: screenSize.width * 0.04,
                                      right: screenSize.width * 0.08),
                                  child: Icon(Icons.search,
                                      color: Color(0xff6644CC)),
                                ),
                                Expanded(
                                    child: InkWell(
                                  onTap: () {
                                    Navigator.push(context,
                                        MaterialPageRoute(builder: (context) {
                                      return SearchScreen(
                                        currentUser: currentUser,
                                      );
                                    }));
                                  },
                                  child: Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        "Search Restaurant",
                                        style: GoogleFonts.montserrat(
                                            fontSize: 15,
                                            fontWeight: FontWeight.w300,
                                            color: Color(0xff707070)),
                                      )),
                                ))
                              ],
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: screenSize.width * 0.055),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              InkWell(
                                onTap: () {
                                  Navigator.push(context,
                                      MaterialPageRoute(builder: (context) {
                                    return SearchScreen();
                                  }));
                                },
                                child: activityButton(screenSize,
                                    "New Reservation", Icons.restaurant),
                              ),
                              InkWell(
                                  onTap: () {
                                    widget.controller.jumpToTab(1);
                                    // pushNewScreen(context,
                                    //     screen: ResHisScreen(), withNavBar: true);
                                  },
                                  child: activityButton(screenSize, "History",
                                      Icons.info_outline)),
                            ],
                          ),
                        ),
                        StreamBuilder<List<Restaurant>>(
                          stream: resProvider.listofRestauants,
                          builder: (BuildContext context,
                              AsyncSnapshot<dynamic> snapshot) {
                            // builder: ( context, snapshot) {
                            if (snapshot.hasData) {
                              restaurantList = snapshot.data;
                              return Padding(
                                padding: EdgeInsets.only(
                                    top: screenSize.height * 0.02),
                                child: Container(
                                  height: screenSize.height * 0.3,
                                  child: Swiper(
                                    index: _currentIndex,
                                    controller: _swiperController,
                                    itemCount: _pageCount,
                                    onIndexChanged: (index) {
                                      setState(() {
                                        _currentIndex = index;
                                      });
                                    },
                                    autoplay: true,
                                    // loop: false,
                                    itemBuilder: (context, index) {
                                      return _buildPage(
                                          titles: restaurantList[index]
                                              .restaurantName,
                                          locations: restaurantList[index]
                                              .restaurantLocation,
                                          images: restaurantList[index]
                                              .restaurantPicUrl);
                                    },
                                    pagination: SwiperPagination(
                                        builder: CustomPaginationBuilder(
                                            activeSize: Size(18.0, 18.0),
                                            activeColor: Colors.black,
                                            size: Size(12.0, 12.0),
                                            color: Color(0xff707070))),
                                  ),
                                ),
                              );
                            } else {
                              return Text("No suggestions enlisted T.T");
                            }
                          },
                        ),
                        Divider(
                          indent: screenSize.width * 0.055,
                          endIndent: screenSize.width * 0.055,
                          thickness: 1,
                          color: Color(0xff8788A8),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: screenSize.height * 0.01,
                              horizontal: screenSize.width * 0.055),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("All Categories",
                                  style: GoogleFonts.raleway(
                                      fontSize: 22,
                                      fontWeight: FontWeight.w700,
                                      color: Color(0xff8788A8))),
                              InkWell(
                                onTap: () {
                                  Navigator.push(context,
                                      MaterialPageRoute(builder: (context) {
                                    return AllCatsScreen(
                                      currentUser: currentUser,
                                    );
                                  }));
                                },
                                child: Text("View all",
                                    style: GoogleFonts.raleway(
                                        fontSize: 13,
                                        fontWeight: FontWeight.w300,
                                        color: Color(0xff8788A8))),
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: screenSize.height * 0.01),
                          child: Container(
                            height: screenSize.height * 0.13,
                            width: screenSize.width,
                            child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: 3,
                                itemBuilder: (BuildContext context, int index) {
                                  return InkWell(
                                    onTap: () {
                                      pushNewScreen(
                                        context,
                                        screen: CatsResScreen(
                                            currentUser: currentUser,
                                            categoryName: categories[index]),
                                        withNavBar: false,
                                      );
                                    },
                                    child: Container(
                                      width: screenSize.width * 0.38,
                                      margin: EdgeInsets.symmetric(
                                          horizontal: 20.0),
                                      decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(20.0),
                                        color: Colors.grey,
                                        image: DecorationImage(
                                            image: AssetImage(images[index]),
                                            fit: BoxFit.cover,
                                            colorFilter: ColorFilter.mode(
                                                Color(0xAA000000),
                                                BlendMode.dstATop)),
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.all(15.0),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              categories[index],
                                              textAlign: TextAlign.start,
                                              style: GoogleFonts.openSans(
                                                  fontWeight: FontWeight.w700,
                                                  fontSize: 20.0,
                                                  color: Colors.white),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  );
                                }),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: screenSize.height * 0.01,
                              horizontal: screenSize.width * 0.055),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text("Popular Restaurants",
                                style: GoogleFonts.raleway(
                                    fontSize: 22,
                                    fontWeight: FontWeight.w700,
                                    color: Color(0xff8788A8))),
                          ),
                        ),
                        StreamBuilder<List<Restaurant>>(
                            stream: resProvider.listofRestauants,
                            builder: (context, snapshot) {
                              if (snapshot.hasData) {
                                restaurantList = snapshot.data;
                                return Padding(
                                  padding: EdgeInsets.symmetric(
                                      vertical: screenSize.height * 0.01),
                                  child: Container(
                                    height: screenSize.height * 0.4,
                                    width: screenSize.width,
                                    child: ListView.builder(
                                        scrollDirection: Axis.horizontal,
                                        itemCount: restaurantList.length,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return InkWell(
                                              onTap: () {
                                                pushNewScreen(
                                                  context,
                                                  screen: ResDetailScreen(
                                                      currentUser: currentUser,
                                                      currentRestaurant:
                                                          restaurantList[
                                                              index]),
                                                  withNavBar: false,
                                                );
                                              },
                                              child: Container(
                                                width: screenSize.width * 0.5,
                                                margin: EdgeInsets.symmetric(
                                                    horizontal: 20.0),
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          20.0),
                                                  color: Colors.grey,
                                                  image: DecorationImage(
                                                      image: NetworkImage(
                                                          restaurantList[index]
                                                              .restaurantPicUrl),
                                                      fit: BoxFit.cover,
                                                      colorFilter:
                                                          ColorFilter.mode(
                                                              Color(0xAA000000),
                                                              BlendMode
                                                                  .dstATop)),
                                                ),
                                                child: Padding(
                                                  padding: const EdgeInsets.all(
                                                      15.0),
                                                  child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.end,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      Text(
                                                        restaurantList[index]
                                                            .restaurantName,
                                                        textAlign:
                                                            TextAlign.start,
                                                        style: GoogleFonts
                                                            .montserrat(
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w500,
                                                                fontSize: 18.0,
                                                                color: Colors
                                                                    .white),
                                                      ),
                                                      Text(
                                                        restaurantList[index]
                                                            .restaurantLocation,
                                                        textAlign:
                                                            TextAlign.start,
                                                        style: GoogleFonts
                                                            .openSans(
                                                                fontSize: 15,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                                color: Colors
                                                                    .white),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ));
                                        }),
                                  ),
                                );
                              } else {
                                return Text("No restaurant enlisted T.T");
                              }
                            }),
                      ],
                    ),
                  ),
                ));
          } else {
            return Center(child: CircularProgressIndicator());
          }
        });
  }

  Widget activityButton(Size screenSize, String activityName, IconData icon) {
    return Container(
        height: screenSize.height * 0.08,
        width: screenSize.width * 0.4,
        padding: EdgeInsets.symmetric(horizontal: screenSize.width * 0.03),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15.0),
            border: Border.all(color: Colors.white),
            boxShadow: [
              BoxShadow(
                color: Color(0x3C000000),
                blurRadius: 3,
              ),
            ]),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            CircleAvatar(
                radius: screenSize.width * 0.06,
                backgroundColor: Color(0xffF5F5DC),
                child: Icon(
                  icon,
                  color: Colors.black,
                )),
            Expanded(
              child: Text(
                activityName,
                style: GoogleFonts.montserrat(
                    fontSize: 12, fontWeight: FontWeight.w300),
                maxLines: 2,
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ));
  }

  Widget _buildPage({String titles, String locations, String images}) {
    final TextStyle titleStyle =
        GoogleFonts.montserrat(fontWeight: FontWeight.w500, fontSize: 18.0);
    return Container(
      width: double.infinity,
      margin: EdgeInsets.only(right: 25.0, bottom: 40.0, left: 25.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30.0),
        color: Colors.grey,
        image: DecorationImage(
            image: NetworkImage(images),
            fit: BoxFit.cover,
            colorFilter:
                ColorFilter.mode(Color(0xAA000000), BlendMode.dstATop)),
      ),
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              titles,
              textAlign: TextAlign.start,
              style: titleStyle.copyWith(color: Colors.white),
            ),
            Text(
              locations,
              textAlign: TextAlign.start,
              style: GoogleFonts.openSans(
                  fontSize: 15,
                  fontWeight: FontWeight.w400,
                  color: Colors.white),
            ),
          ],
        ),
      ),
    );
  }
}
