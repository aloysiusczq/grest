import 'package:flutter/material.dart';

final List<String> categories = [
  "Casual Dining",
  "Cafe",
  "Fast Casual",
  "Fine Dining",
  "Fast Food",
  "Chinese",
  "Korean",
  "Japanese",
  "Western",
  "Indian",
  "Malay",
  "Vegan"
];

final List<Color> catColor = [
  Color(0xffA8A887),
  Color(0xffF5F5DC),
  Color(0xffFFFFBF),
  Color(0xff707070),
  Color(0xffFFA200),
  Color(0xffFFF6F6),
  Color(0xff4C4CF8),
  Color(0xffFF7F7F),
  Color(0xffD3D3D3),
  Color(0xff866949),
  Color(0xff00720F),
  Color(0xffA1BF36),
];

final List<Color> catTextColor = [
  Colors.white,
  Colors.black,
  Colors.black,
  Colors.white,
  Colors.white,
  Colors.black,
  Colors.white,
  Colors.white,
  Colors.black,
  Colors.white,
  Colors.white,
  Colors.white,
];

String currentUserId;
