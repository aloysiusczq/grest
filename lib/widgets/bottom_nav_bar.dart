import 'package:flutter/material.dart';
import '../screens/home_screen.dart';
import 'package:grest/screens/reservation_history_screen.dart';
import 'package:grest/screens/user_profile.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class NavTab extends StatefulWidget {
  static String routeName = "nav_tab";

  @override
  _NavTabState createState() => _NavTabState();
}

class _NavTabState extends State<NavTab> {
  PersistentTabController _controller;
  bool _hideNavBar;

  @override
  void initState() {
    super.initState();
    _controller = PersistentTabController(initialIndex: 0);
    _hideNavBar = false;
  }

  List<Widget> _buildScreens(PersistentTabController _controller) {
    return [
      HomeScreen(controller: _controller),
      ResHisScreen(),
      UserProfileScreen(),
    ];
  }

  List<PersistentBottomNavBarItem> _navBarsItems() {
    return [
      PersistentBottomNavBarItem(
        icon: Icon(Icons.home),
        activeColorPrimary: Color(0xffA887A6),
        inactiveColorPrimary: Color(0xff000000),
      ),
      PersistentBottomNavBarItem(
        icon: Icon(Icons.info_outline),
        activeColorPrimary: Color(0xffA887A6),
        inactiveColorPrimary: Color(0xff000000),
      ),
      PersistentBottomNavBarItem(
        icon: Icon(Icons.account_circle_outlined),
        activeColorPrimary: Color(0xffA887A6),
        inactiveColorPrimary: Color(0xff000000),
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return PersistentTabView(
      context,
      controller: _controller,
      screens: _buildScreens(_controller),
      items: _navBarsItems(),
      confineInSafeArea: true,
      backgroundColor: Colors.white,
      handleAndroidBackButtonPress: true,
      resizeToAvoidBottomInset: true,
      stateManagement: true,
      hideNavigationBarWhenKeyboardShows: true,
      hideNavigationBar: _hideNavBar,
      popAllScreensOnTapOfSelectedTab: true,
      itemAnimationProperties: ItemAnimationProperties(
        duration: Duration(milliseconds: 400),
        curve: Curves.ease,
      ),
      screenTransitionAnimation: ScreenTransitionAnimation(
        animateTabTransition: true,
        curve: Curves.ease,
        duration: Duration(milliseconds: 200),
      ),
      navBarStyle: NavBarStyle.style1,
    );
  }
}
