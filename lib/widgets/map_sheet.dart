import 'package:flutter/material.dart';
import 'package:map_launcher/map_launcher.dart';

class MapsSheet {
  static show({
    @required BuildContext context,
    @required Function(AvailableMap map) onMapTap,
  }) async {
    final availableMaps = await MapLauncher.installedMaps;

    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return SafeArea(
          child: Container(
            height: 300,
            child: SingleChildScrollView(
              child: Container(
                child: Wrap(
                  children: <Widget>[
                    for (var map in availableMaps)
                      ListTile(
                          onTap: () => onMapTap(map),
                          title: Text(map.mapName),
                          leading: Icon(Icons.map)),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
